<?php

use Carbon\Carbon;
use App\Models\Employee;

$loan_exceed_count = Employee::where('loan_amount', '>', 0)->get()->count();
?>
<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<li class="nav-item"><a class="nav-link" href="{{ backpack_url('dashboard') }}"><i class="las la-chart-line nav-icon"></i>အကျဉ်းချုပ်</a></li>
<li class="nav-title">အခြေခံအချက်အလက်များ</li>

<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-clipboard"></i>အခြေခံအချက်များ</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('category') }}'><i class='nav-icon las la-tags'></i> အမှတ်တံဆိပ်စာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('supplier') }}'><i class='nav-icon las la-people-carry'></i>ဝယ်သူစာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('employee') }}'><i class='nav-icon las la-user-edit'></i>ဝန်ထမ်းစာရင်း</a></li>
        <li class="nav-item"><a class="nav-link" href="{{ backpack_url('elfinder') }}" target="_blank"><i class="nav-icon la la-files-o"></i> <span>ဖိုင်တင်ရန်</span></a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('shop') }}'><i class='nav-icon las la-store'></i> ပိတ်ဝယ်ဆိုင်များ</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('unit') }}'><i class='nav-icon las la-balance-scale-left'></i> အတိုင်းအတာများ</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('expense-type') }}'><i class='nav-icon las la-sign-out-alt'></i> Expense types</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ot-bonus') }}'><i class='nav-icon las la-stopwatch'></i>OT Bouns</a></li>
    </ul>
</li>


<li class="nav-title">စာရင်းများ</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-clipboard"></i>စာရင်းထည့်မည်</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('catalogue') }}'><i class='nav-icon las la-tshirt'></i> ဒီဇိုင်းများ</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ahtal-pop') }}'><i class='nav-icon las la-truck'></i>အထည်ပို့စာရင်း</a></li>
        <!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('htal-pop') }}'><i class='nav-icon la la-question'></i> အထည်ပို့စာရင်း</a></li> -->
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pate-nyut') }}'><i class='nav-icon las la-cut'></i> ပိတ်ညှပ်စာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('pate-wal') }}'><i class='nav-icon las la-book-open'></i> ပိတ်ဝယ်စာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('expense') }}'><i class='nav-icon las la-sign-out-alt'></i> အထွက်စာရင်း</a></li>
        <!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('cashbook') }}'><i class='nav-icon las la-book-open'></i> အကြွေး/ဆပ် စာရင်း</a></li> -->
        <li class='nav-item'><a class='nav-link' href='{{route('attendence',['date'=>Carbon::now()->toDateString()])}}'><i class='nav-icon las la-calendar'></i>အလုပ်တက်စာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('otextra') }}'><i class='nav-icon las la-clock'></i>OT Extra</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('staff-loan') }}'><i class='nav-icon las la-money-bill-wave-alt'></i> ကြိုငွေထည့်ရန်</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('paystaffloan') }}'><i class='nav-icon las la-book-open'></i> ကြိုငွေစာရင်း
                @if($loan_exceed_count == 0)
                <span class="badge badge-pill bg-success">0</span>
                @else
                <span class="badge badge-pill bg-danger">{{$loan_exceed_count}}</span>
                @endif
            </a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-clipboard"></i>စုငွေစာရင်း</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('financial-account') }}'><i class='nav-icon las la-landmark'></i> စုငွေအကောင့်များ </a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('su-boo') }}'><i class='nav-icon las la-donate'></i> စုငွေထည့်ရန် </a></li>
    </ul>
</li>

<li class="nav-title">Reportများထုတ်/ကြည့်ရန်</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-clipboard"></i>စာရင်းကြည့်မည်</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('attendence') }}'><i class='nav-icon las la-calendar'></i>အလုပ်တက်စာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ route('salarylist',['date'=>Carbon::now()->toDateString()])}}'><i class='nav-icon las la-clipboard'></i>လစာစာရင်း</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ route('paySlip',['date'=>Carbon::now()->toDateString()])}}'><i class='nav-icon las la-clipboard'></i> Pay Slip ထုတ်ရန်</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('salary') }}'><i class='nav-icon las la-clipboard'></i> လစာစာရင်းပြင်ရန်</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('salary-log') }}'><i class='nav-icon las la-chart-bar'></i> Salary logs</a></li>
    </ul>
</li>
<li class="nav-item nav-dropdown">
    <a class="nav-link nav-dropdown-toggle" href="#"><i class="nav-icon las la-clipboard"></i>Reports</a>
    <ul class="nav-dropdown-items">
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ledger/create') }}'><i class='nav-icon las la-book-open'></i> စာရင်းချုပ်</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('htalpopreport/create') }}'><i class='nav-icon las la-print'></i> အထည်ပို့စာရင်းချုပ်</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('salaryreport/create') }}'><i class='nav-icon las la-print'></i> Salary Report</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('employeereport/create') }}'><i class='nav-icon las la-print'></i> Employee Reports</a></li>
        <li class='nav-item'><a class='nav-link' href='{{ backpack_url('employee-detail/create') }}'><i class='nav-icon las la-info-circle'></i> Employee Infos</a></li>
    </ul>
</li>


<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('ledger') }}'><i class='nav-icon la la-question'></i> Ledgers</a></li> -->
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('bonus') }}'><i class='nav-icon las la-money-bill-wave-alt'></i> Bonus</a></li>
<!-- <li class='nav-item'><a class='nav-link' href='{{ backpack_url('bonus-employee') }}'><i class='nav-icon la la-question'></i> Bonus employees</a></li> -->
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('give-bonus/create') }}'><i class='nav-icon las la-money-bill-wave-alt'></i> Give bonuses</a></li>
<li class='nav-item'><a class='nav-link' href='{{ backpack_url('leave-list/create') }}'><i class='nav-icon las la-chart-bar'></i> Leave lists</a></li>