@extends(backpack_view('blank'))
<?php
    use App\Models\Attendence;
    use App\Models\Employee;
    $count     = 1;
?>
@section('content')
<style>
    input.form-control {
  width: auto;
}

</style>
<div class="container">
  <div class="row">
    <div class="col-12 table-responsive">
    <h4 class="text-center mt-3 mb-5">အလုပ်တက်/ပျက် စာရင်း({{$date}})</h4>
    <form method="get" action="{{route('attendence')}}">
        @csrf
        <div class="row">
          <div class="col-md-9"></div>
          <div class="col-md-3">
            <div class="input-group mb-3">
            <input type="date" class="form-control" name="date" id="exampleInputEmail1" value="{{$date}}">
            <div class="input-group-prepend">
              <button class="btn btn-outline-primary" type="submit"><i class="lab la-searchengin"></i></button>
            </div>
          </div>
          </div>
        </div>
        
    </form>

      <table class="table table-bordered table-striped mt-3">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">ဝန်ထမ်းအမည်</th>
            <th scope="col">မနက်ပိုင်း</th>
            <th scope="col">ညနေပိုင်း</th>
            <th scope="col">အလုပ်ချိန်</th>
            <th scope="col">အချိန်ပိုထည့်ရန်</th>
          </tr>
        </thead>
        <tbody>
          @foreach($employees as $emp)
          @php
            $attendence = Attendence::where('employee_id',$emp->id)->whereDate('attendence_date',$date)->first();
            $att_morning = "";
            $att_evening = "";
            $overtime = "";
            $worktime = 0;
            if($attendence)
            {
              $worktime = $attendence->worktime;
              $overtime = $attendence->overtime;
              if($attendence->morning == 1)
              {
                $att_morning = "checked";
              }

              if($attendence->evening == "1")
              {
                $att_evening = "checked";
              }
            }
          @endphp
          <tr>
            <td>{{$count++}}</td>
            <td>{{$emp->name}}</td>
            <td>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input morning" data-date="{{$date}}" data-id="{{$emp->id}}" id="cusCheck1{{$emp->id}}" {{$att_morning}}>
                  <label class="custom-control-label" for="cusCheck1{{$emp->id}}"></label>
              </div>
            </td>
            <td>
              <div class="custom-control custom-checkbox">
                  <input type="checkbox" class="custom-control-input evening" data-date="{{$date}}" data-id="{{$emp->id}}" id="cusCheck2{{$emp->id}}" {{$att_evening}}>
                  <label class="custom-control-label" for="cusCheck2{{$emp->id}}"></label>
              </div>
            </td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="number" id="worktime" data-id="{{$emp->id}}" data-date="{{$date}}" class="form-control time time{{$emp->id}}" placeholder="အချိန်ထည့်ရန်" value="{{$worktime}}" max="8" min="0" step="0.5"/>
                </div>
            </td>
            <td>
                <div class="custom-control custom-checkbox">
                    <input type="text" id="overtime" data-id="{{$emp->id}}" data-date="{{$date}}" class="form-control ot" placeholder="အချိန်ပိုထည့်ရန်" value="{{$overtime}}"/>
                </div>
            </td>
         
          </tr>
          @endforeach
          
      
        </tbody>
      </table>
    </div>
  </div>
</div>
@endsection
@section('after_scripts')
<script>
    $(document).ready(function()
    {
      var token = '{{csrf_token()}}';

      $('.morning').on('click',function()
      {
        if($(this).prop("checked")) {
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          var status = 1;
          var worktime = $(".time"+emp_id).val();
          $(".time"+emp_id).val(parseFloat(worktime) + 4);
          var realtime =  parseFloat(worktime) + 4;

        } else {
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          var status = 0;
          var worktime = $(".time"+emp_id).val();
          $(".time"+emp_id).val( parseFloat(worktime) - 4);
          var realtime =  parseFloat(worktime) - 4;
        }
        $.ajax({
            url: '{{route('morning')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                emp_id,
                status,
                date,
                realtime
            },
            success: function(responese) {
                
            },
        });
      })


      $('.evening').on('click',function()
      {
        if($(this).prop('checked')) {
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          var status = 1;
          var worktime = $(".time"+emp_id).val();
          $(".time"+emp_id).val( parseFloat(worktime) + 4);
          var realtime =  parseFloat(worktime) + 4;

        } else {
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          var status = 0;
          var worktime = $(".time"+emp_id).val();
          $(".time"+emp_id).val( parseFloat(worktime) - 4);
          var realtime =  parseFloat(worktime) - 4;

        }
        $.ajax({
            url: '{{route('evening')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                emp_id,
                status,
                date,
                realtime
            },
            success: function(responese) {
                
            },
        });
      })

      $('.ot').on('keyup',function(){
          var overtime = $(this).val();
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          $.ajax({
            url: '{{route('overtime')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                emp_id,
                overtime,
                date
            },
            success: function(responese) {
                
            },
        });
      })
      $('.time').on('change',function(){
          var timeTotal = $(this).val();
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          $.ajax({
            url: '{{route('workingtime')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                emp_id,
                timeTotal,
                date
            },
            success: function(responese) {
                
            },
        });
      })

      $('.time').on('keyup',function(){
          var timeTotal = $(this).val();
          var emp_id = $(this).data('id');
          var date = $(this).data('date');
          $.ajax({
            url: '{{route('workingtime')}}',
            type: 'POST',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            data: {
                emp_id,
                timeTotal,
                date
            },
            success: function(responese) {
                
            },
        });
      })
    })
</script>
@endsection