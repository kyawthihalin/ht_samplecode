<div class="table-responsive" id="attendance_table">
    <table class="table table-bordered table-striped">
        <thead>
            <th>စဉ်</th>
            <th>အမည်</th>
            @foreach ($periods as $period)
            <th class="text-center @if($period->format('D') == 'Sun') bg-warning @endif">{{$period->format('d')}} <br> {{$period->format('D')}}</th>
            @endforeach
            <th class="text-center">အချိန်ပိုစုစုပေါင်း</th>
            <th class="text-center">ပျက်ရက်စုစုပေါင်း</th>
        </thead>
        <tbody>
        @php 
            $count = 1;
        @endphp
        @foreach ($employees as $employee)
            @php
                $total_ot = 0;
                $leave = 0;
                $leavetime = 0;
            @endphp
            <tr>
                <td>{{$count++}}</td>
                <td>{{$employee->name}}</td>
                @foreach ($periods as $period)
                    @php
                        $m_icon = '';
                        $e_icon = '';
                        $overtime = "-";
                        $attendance = collect($attendances)->where('employee_id', $employee->id)->where('attendence_date',
                        $period->format('Y-m-d'))->first();
                        if($attendance)
                        {
                            if($attendance->overtime == null)
                            {
                            }
                            else
                            {
                                $overtime = $attendance->overtime;
                                $total_ot += $attendance->overtime;
                            }
                            if($period->format('D') == 'Sun')
                            {

                            }
                            else
                            {
                                if($attendance->morning == 0 || $attendance->morning == null)
                                {
                                    $leave = $leave + 0.5;
                                }
                                if($attendance->evening == 0 || $attendance->evening == null)
                                {
                                    $leave = $leave + 0.5;
                                }
                                if($attendance->morning == 1 && $attendance->evening == 1)
                                {
                                    $worktime = $attendance->worktime;
                                    if($worktime < 8)
                                    {
                                        $leavetime += 8 - $worktime;
                                    }
                                }
                            }
                          
                        }
                        else{
                            if($period->format('D') == 'Sun')
                            {

                            }
                            else
                            {
                                $leave = $leave + 1;
                            }
                        }
                    @endphp
                <td class="text-center @if($period->format('D') == 'Sun') alert-warning @endif">
                    @if($attendance)
                    @if($attendance->morning == 1)
                    <div> <img src="{{asset('images/true.png')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    @else
                    <div> <img src="{{asset('images/false.gif')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    @endif

                    @if($attendance->evening == 1)
                    <div> <img src="{{asset('images/true.png')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    @else
                    <div> <img src="{{asset('images/false.gif')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    @endif

                    @else
                    <div> <img src="{{asset('images/false.gif')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    <div> <img src="{{asset('images/false.gif')}}" alt="" srcset="" width="20px" height="20px"> </div>
                    @endif
                    @if($overtime > 0)
                    <div>{{$overtime}} <small>hours</small> </div>
                    @else
                    <div>- <small>hours</small> </div>
                    @endif
                </td>
              
                @endforeach
                @php
                    if($leave >= 24)
                    {
                        $leave = $leave + 4;
                    }elseif($leave >= 18)
                    {
                        $leave = $leave + 3;
                    }elseif($leave >= 12)
                    {
                        $leave = $leave + 2;
                    }elseif($leave >= 6)
                    {
                        $leave = $leave + 1;
                    }
                @endphp
                <td>   
                    @if($total_ot == 0 || $total_ot == null)
                    <div>OT မရှိပါ</div>
                    @else
                    <div>{{$total_ot}}နာရီ</div>
                    @endif
                </td>
                <td>
                    @if($leave == 0 && $leavetime == 0)
                    <div>ပျက်ရက် မရှိပါ</div>
                    @else
                    <div>{{$leave}}ရက် {{$leavetime}}နာရီ</div>
                    @endif
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>