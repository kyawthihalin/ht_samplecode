<?php
    use App\Models\Employee;
    $employees = Employee::where('stauts','Active')->get();
?>
@extends(backpack_view('blank'))
@push('after_styles')
<meta charset="utf-8">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-md-3">
            <div class="form-group">
                    <select name="" class="form-control employee_name">
                        <option value="">-- Please Choose Employee --</option>
                        @foreach($employees as $emp)
                            <option value="{{$emp->id}}">{{$emp->name}}</option>
                        @endforeach
                       
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select name="" class="form-control select-month">
                        <option value="">-- Please Choose (Month) --</option>
                        <option value="01" @if(now()->format('m') == '01') selected @endif>Jan</option>
                        <option value="02" @if(now()->format('m') == '02') selected @endif>Feb</option>
                        <option value="03" @if(now()->format('m') == '03') selected @endif>Mar</option>
                        <option value="04" @if(now()->format('m') == '04') selected @endif>Apr</option>
                        <option value="05" @if(now()->format('m') == '05') selected @endif>May</option>
                        <option value="06" @if(now()->format('m') == '06') selected @endif>Jun</option>
                        <option value="07" @if(now()->format('m') == '07') selected @endif>Jul</option>
                        <option value="08" @if(now()->format('m') == '08') selected @endif>Aug</option>
                        <option value="09" @if(now()->format('m') == '09') selected @endif>Sep</option>
                        <option value="10" @if(now()->format('m') == '10') selected @endif>Oct</option>
                        <option value="11" @if(now()->format('m') == '11') selected @endif>Nov</option>
                        <option value="12" @if(now()->format('m') == '12') selected @endif>Dec</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <select name="" class="form-control select-year">
                        <option value="">-- Please Choose (Year) --</option>
                        @for ($i = 0; $i < 5; $i++)
                        <option value="{{now()->subYears($i)->format('Y')}}" @if(now()->format('Y') == now()->subYears($i)->format('Y')) selected @endif>
                            {{now()->subYears($i)->format('Y')}}
                        </option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-2">
            <button class="btn btn-danger btn-sm btn-block search-btn"><i class="las la-search"></i> Search</button>
            </div>
            <div class="col-md-1">
                <a href="#" class="btn btn-success btn-sm btn-block" id="print"><i class="las la-print"></i>Print</a>
            </div>
        </div>
        <div class="attendance_overview_table" id="attendance_overview_table"></div>
    </div>
</div>
@endsection
@push('after_scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script>
    $(document).ready(function(){
        var token = '{{csrf_token()}}';
        $('.select-month').select2({
            placeholder: '-- Please Choose (Month) --',
            allowClear: true,
        });
        $('.select-year').select2({
            placeholder: '-- Please Choose (Year) --',
            allowClear: true,
        });
        $('.employee_name').select2({
            placeholder: '-- Please Choose (Employee) --',
            allowClear: true,
        });
        attendanceOverviewTable();
        function attendanceOverviewTable(){
            var employee_name = $('.employee_name').val();
            var month = $('.select-month').val();
            var year = $('.select-year').val();
            console.log(month);
            console.log(year);
            if(month == "" || year == "")
            {
                alert('လနှင့်နှစ်ကိုရွေးပါ');
            }
            else
            {
                $.ajax({
                    url: '{{route('attendence_overview')}}',
                    type: 'POST',
                    async: false,
                    headers: {
                        'X-CSRF-Token': token
                    },
                    data: {
                        employee_name,
                        month,
                        year
                    },
                    success: function(res){
                        $('.attendance_overview_table').html(res);
                    }
                 });
            }
           
        }
        $('.search-btn').on('click', function(event){
            event.preventDefault();
            attendanceOverviewTable();
        });

        $('#print').on('click',function()
        {
            
            var printContent = document.getElementById("attendance_overview_table");
            var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'padding:0.5em;' +
            '}' +
            'img {' +
                '-webkit-print-color-adjust: exact;'+
            '}'+
            '</style>';
            htmlToPrint += printContent.outerHTML;
            var WinPrint = window.open("");
            WinPrint.document.write(htmlToPrint);
         

            setTimeout(function(){
                WinPrint.print();
                WinPrint.close();
            },100);
            newWin.document.write('<html><body onload="window.print()" style="text-align:center;margin-top:50%">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

        })
    });
</script>
@endpush