<style>
    table {
        width: 100%;
    }

    table,
    th,
    td {
        border-collapse: collapse;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
        text-align: left;
    }
</style>
@if(count($employees) > 0)
@foreach($employees as $emp)
<h3 style="text-align:center;font-weight:bold">H & T Fashion Curriculum Vitae</h3>
<table>
    <tbody>
        <tr>
            <td>
                @if($emp->employee_photo)
                <img style="width:100px;height: 100px;" src="{{asset($emp->employee_photo)}}" alt="">
                @else
                -
                @endif
            </td>
        </tr>
        <tr>
            <td>အမည် - </td>
            @if($emp->name)
            <td>{{$emp->name}}</td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td>ဖုန်းနံပါတ် - </td>
            @if($emp->phone)
            <td>{{$emp->phone}}</td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td>မှတ်ပုံတင်အမှတ် - </td>
            @if($emp->nrc_number)
            <td>
                {{$emp->nrc_number}}
            </td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td>လိပ်စာ - </td>
            @if($emp->address)
            <td>
                {{$emp->address}}
            </td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td>Date of Birth - </td>
            @if($emp->dob)
            <td>
                {{$emp->dob}}
            </td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td>အလုပ်၀င်သည့်ရက်စွဲ - </td>
            @if($emp->joined_date)
            <td>
                {{$emp->joined_date}}
            </td>
            @else
            <td>-</td>
            @endif
        </tr>
        <tr>
            <td style="padding-top:20px;padding-bottom:20px;">
                <h2 style="font-weight: bold;">NRC Photos</h2>
            </td>
        </tr>
        <tr>
            <td>
                @if($emp->nrc_front)
                <img style="width:300px;height: 300px;" src="{{asset($emp->nrc_front)}}" alt="">
                @else
                -
                @endif
            </td>
            <td>
                @if($emp->nrc_back)
                <img style="width:300px;height: 300px;" src="{{asset($emp->nrc_back)}}" alt="">
                @else
                -
                @endif
            </td>
        </tr>
    </tbody>

</table>
<p style="page-break-before: always;">&nbsp;</p>
@endforeach
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}">
<h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif