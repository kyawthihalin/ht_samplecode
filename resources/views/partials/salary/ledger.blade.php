<div style="text-align: center">
    <button type="button" class="btn btn-app search-data">
        <i class="la la-search"></i>Search
    </button>
    <button type="button" class="btn btn-app print-data">
        <i class="la la-print"></i>Print
    </button>

</div>
<br>
<div class="col-md-12 show-report" id="DivIdToPrint" style=" width: 100%; background-color: #ffffff; padding: 20px; -webkit-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
-moz-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48); border-radius: 5px;overflow: scroll;"></div>

@push('crud_fields_styles')
<style>
     li.active a {
            background-color: #222d32 !important;
        }

        .table-data {
            width: 100%;
        }

        .table-data, .table-data th, .table-data td {
            border-collapse: collapse;
            border: 1px solid #a8a8a8;
        }

        .table-data th {
            text-align: center;
            padding: 5px;
        }

        .table-data td {
            padding: 5px;
        }

        .table-data tbody > tr:nth-child(odd) {
            color: #606060;
        }

        .show-report {
            max-height: 640px;
            min-height: 640px;
            overflow: auto;
        }
</style>
@endpush
@push('crud_fields_scripts')
<script>
    var token = '{{csrf_token()}}';
    function exportTableToExcel(example, filename = '') {
        var downloadLink;
        var dataType = 'application/vnd.ms-excel';
        var tableSelect = document.getElementById(example);
        var tableHTML = tableSelect.outerHTML.replace(/ /g, '%20');

        // Specify file name
        filename = filename ? filename + '.xls' : 'excel_data.xls';

        // Create download link element
        downloadLink = document.createElement("a");

        document.body.appendChild(downloadLink);

        if (navigator.msSaveOrOpenBlob) {
            var blob = new Blob(['\ufeff', tableHTML], {
                type: dataType
            });
            navigator.msSaveOrOpenBlob(blob, filename);
        } else {
            // Create a link to the file
            downloadLink.href = 'data:' + dataType + ', ' + tableHTML;

            // Setting the file name
            downloadLink.download = filename;

            //triggering the function
            downloadLink.click();
        }
    }
    $(document).ready(function() {
        $('#saveActions').hide();
        $('small').hide();
        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");
    })
    $('.print-data').on('click', function() {
        printDiv();
    });
    $('.search-data').on('click', function(e) {
        e.preventDefault();
        var end_date = $('[name="Edate"]').val();
        var start_date = $('[name="Sdate"]').val();
        if(end_date == null || end_date == "" || start_date == null || start_date == "")
        {
        
            alert('Please Select Start Date and End Date');

        }
        else
        {
            
            $.ajax({
                url: '{{route('Lreport')}}',
                type: 'POST',
                async: false,
                headers: {
                    'X-CSRF-Token': token
                },
                data: {
                    end_date,
                    start_date
                },
                success: function(res){
                    $('.show-report').html(res);
                },
                error:function(res)
                {
                    alert("Please Select Month and Year");
                }
            })
        }
    });

    function printDiv() {

        var printContent = document.getElementById("attendance_table");
            var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'padding:0.5em;' +
            
            '}' +
            'table {' +
                'width:100%;'+
            '}'+
            '</style>';
            htmlToPrint += printContent.outerHTML;
            var WinPrint = window.open("");
            WinPrint.document.write(htmlToPrint);
         

            setTimeout(function(){
                WinPrint.print();
                WinPrint.close();
            },100);
            newWin.document.write('<html><body onload="window.print()" style="text-align:center;margin-top:50%">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();
    }
</script>
@endpush