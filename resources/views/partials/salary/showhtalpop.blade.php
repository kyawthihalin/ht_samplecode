<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }

</style>
@if(count($htal_pops) > 0)
<table>
    <thead>
        <th>No</th>
        <th>Code Number</th>
        <th>Date</th>
        <th>Customer Name</th>
        <th>တံဆိပ်</th>
        <th>အရေအတွက်</th>
        <th>ကျသင့်ငွေ</th>
        <th>ကြွေးကျန်ရရှိ</th>
        <th>Kpay</th>
        <th>ကြွေးကျန်</th>
    </thead>
    
    <tbody>
    @php
        $count = 1;
        $total = 0;
        $totalKpay = 0;
        $allq = 0;
    @endphp
    @foreach($htal_pops as $htal_pop)
    @php 
        $total += $htal_pop->get_amount;
        $totalKpay += $htal_pop->kpay;
        $allq += $htal_pop->quantity;
    @endphp
    <tr>
        <td>{{$count++}}</td>
        <td>{{$htal_pop->code}}</td>
        <td>{{$htal_pop->htal_pop_date}}</td>
        <td>{{$htal_pop->customer->name}}</td>
        <td>{{$htal_pop->tag->name}}</td>
        <td>{{$htal_pop->quantity}}</td>
        <td>{{number_format($htal_pop->total,0)}}</td>
        <td>{{number_format($htal_pop->get_amount,0)}}</td>
        <td>{{number_format($htal_pop->kpay,0)}}</td>
        <td>{{number_format($htal_pop->rest_amount,0)}}</td>
    </tr>
   
    @endforeach
    </tbody>
  
</table>
<br><br>
<table>
    <thead>
    <th> No </th>
    <th>အထည်အရေအတွက်</th>
    <th>အ၀င်</th>
    <th>Kpay</th>
    </thead>
    <tbody>
    <tr>
        <td>
            1
        </td>
        <td>
            {{number_format($allq,0)}}
        </td>
        <td>
            {{number_format($total,0)}}
        </td>
        <td>
            {{number_format($totalKpay,0)}}
        </td>
    </tr>
    </tbody>
</table>
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}"><h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif