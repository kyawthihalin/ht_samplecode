@php
use Carbon\Carbon;
use App\Models\Attendence;
@endphp
<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }

</style>
@if(count($employees) > 0)
<table>
    <thead>
        <th>စဉ်</th>
        <th>အမည်</th>
        <th>Joined Date</th>
        <th>လုပ်သက်</th>
        <th>Total Leave</th>
        <th>Total OT</th>
    </thead>
    <tbody>
    @php
        $count = 1;
    @endphp
    @foreach($employees as $emp)

    @php 
    $attendences = App\Models\Salary::where('employee_id', $emp->id)->where('created_at','>=',$startDate)->where('created_at','<=',$endDate)->get();

        if($emp->joined_date != null || !$emp->joined_date != ""){
            $now = Carbon::now();
            $start_date =  Carbon::parse($emp->joined_date);
            $diff = $now->diff($start_date);
        }
        $leave = 0;
        $leavetime = 0;
        $total_ot =0;
        if($emp->joined_date != null || !$emp->joined_date != ""){
            $now = Carbon::now();
            $start_date =  Carbon::parse($emp->joined_date);
            $diff = $now->diff($start_date);
        }
        foreach($attendences as $attendance)
        {
            $leave += $attendance->total_leave;
            $leavetime += $attendance->leave_time;
            $total_ot += $attendance->total_ot;
        }
        if($leavetime > 8)
        {
            $divided_leave = $leavetime / 8;
            if(is_int($divided_leave))
            {
                $leave += $divided_leave;
            }else{
                $new_leave = floor($leave);
                $leave += $new_leave;
                $new_leave_time = $new_leave * 8;
                $leavetime = $leavetime - $new_leave_time;
            }
        }
    @endphp
    <tr>
        <td>{{$count++}}</td>
        <td>{{$emp->name}}</td>
        @if($emp->joined_date == null || $emp->joined_date == "")
            <td>-</td>
        @else
            <td>{{$emp->joined_date}}</td>
        @endif
        <td>
        {{$diff->y}} Years {{$diff->m}} Months {{$diff->d}} Days
        </td>
        <td>{{$leave}} ရက် {{$leavetime}} နာရီ</td>
        <td>{{$total_ot}} နာရီ</td>
    </tr>
   
    @endforeach
    </tbody>
  
</table>
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}"><h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif