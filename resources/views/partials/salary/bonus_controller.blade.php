@php
use Carbon\Carbon;
@endphp
<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }

</style>
@if(count($employees) > 0)
<table>
    <thead>
        <th>စဉ်</th>
        <th>အမည်</th>
        <th>အလုပ်စဆင်းသည့်နေ့</th>
        <th>လုပ်သက်</th>
        <th>Bonus</th>
    </thead>
    
    <tbody>
    @php
        $count = 1;
        $bonus = 0;
        $total_bonus=0;
    @endphp
    @foreach($employees as $emp)
    @php 

        if($emp->joined_date != null || !$emp->joined_date != ""){
            $now = Carbon::now();
            $start_date =  Carbon::parse($emp->joined_date);
            $diff = $now->diff($start_date);
            $year = $diff->y;
            $month = $diff->m;

            if($year >= 1 && $month >= 6)
            {
                $bonus_for_year = bcmul($year,$bonuses->bonus_per_year);
                $bonus_for_month = bcdiv($bonuses->bonus_per_year,2);
                $bonus = bcadd($bonus_for_year,$bonus_for_month);
            }elseif($year >=1 && $month < 6)
            {
                $bonus = bcmul($year,$bonuses->bonus_per_year);
            }elseif($year < 1 && $month == 11)
            {
                $bonus = $bonuses->eleven_month;

            }elseif($year < 1 && $month == 10)
            {
                $bonus = $bonuses->ten_month;

            }elseif($year < 1 && $month > 6)
            {
                $bonus = bcdiv($bonuses->bonus_per_year,2);
            }elseif($year < 1 && $month < 6)
            {
                $bonus = $bonuses->bonus_under_year;
            }
            $total_bonus += $bonus;
        }
    @endphp
    <tr>
        <td>{{$count++}}</td>
        <td>{{$emp->name}}</td>
        @if($emp->joined_date == null || $emp->joined_date == "")
            <td>-</td>
        @else
            <td>{{$emp->joined_date}}</td>
        @endif
        <td>
        {{$diff->y}} Years {{$diff->m}} Months {{$diff->d}} Days
        </td>
        <td>{{ number_format($bonus,0)}}</td>
    </tr>
   
    @endforeach
    <tr style="font-weight:bold">
        <td colspan="4">Total Salary</td>
        <td>{{number_format($total_bonus,0)}}</td>
    </tr>
    </tbody>
  
</table>
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}"><h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif