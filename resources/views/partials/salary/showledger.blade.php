<div class="table-responsive" id="attendance_table">
    <table class="table table-bordered table-striped">
        <thead class="bg-dark">
            <th>စဉ်</th>
            <th>အမျိုးအစား</th>
            <th>စုစုပေါင်း</th>
        </thead>
        <tbody>
            @php
                $count = 5;
                $total_exp = $pate_nyut + $pate_wal;
                $total = $htal_pop + $htal_pop_kpay;
            @endphp
            <tr>
                <td>1</td>
                <td style="color: green;font-weight:bold">အ၀င်</td>
                <td style="color: green;font-weight:bold">{{number_format($htal_pop)}} ကျပ်</td>
            </tr>
            <tr>
                <td>1</td>
                <td style="color: green;font-weight:bold">Kpay</td>
                <td style="color: green;font-weight:bold">{{number_format($htal_pop_kpay)}} ကျပ်</td>
            </tr>
            <tr>
                <td>2</td>
                <td style="color: red;font-weight:bold">ပိတ်၀ယ်</td>
                <td style="color: red;font-weight:bold">{{number_format($pate_wal)}} ကျပ်</td>
            </tr>
            <tr>
                <td>3</td>
                <td style="color: red;font-weight:bold">ပိတ်ညှပ်</td>
                <td style="color: red;font-weight:bold">{{number_format($pate_nyut)}} ကျပ်</td>
            </tr>
            <tr>
                <td>4</td>
                <td style="color: red;font-weight:bold">ကြိုငွေ</td>
                <td style="color: red;font-weight:bold">{{number_format($loans)}} ကျပ်</td>
            </tr>
            @foreach($expenses as $exp)
                @php
                $total_exp += $exp->total;
                @endphp
                @if($exp->expense_type_id === 3)
                <tr>
                    <td>{{$count++}}</td>

                    <td style="color: red;font-weight:bold">{{$exp->type->type}}(expense)</td>
                    <td style="color: red;font-weight:bold">{{number_format($exp->total)}} ကျပ်</td>
                </tr>
                @else
                <tr>
                    <td>{{$count++}}</td>

                    <td style="color: red;font-weight:bold">{{$exp->type->type}}</td>
                    <td style="color: red;font-weight:bold">{{number_format($exp->total)}} ကျပ်</td>
                </tr>
                @endif
            @endforeach
            <tr class="bg-success">
                <td>{{$count++}}</td>
                <td style="font-weight:bold">Total</td>
                <td style="font-weight:bold">{{ number_format($total - ($total_exp + $loans )) }} ကျပ်</td>
            </tr>
        </tbody>
    </table>
</div>