 <?php
    use App\Models\Employee;  
 ?>
 <style>

table {
    width: 100%;
}

table, table th, table td {
    border-collapse: collapse;
    border: 1px solid #a8a8a8;
}

table th {
    text-align: center;
    padding: 5px;
}

table td {
    padding: 5px;
}

.card_wrapper {
  max-width: 1140px;
  display: block;
  float:left;
  padding: 30px 28px;
}

.card1 {
  display: block;
  float:left;
  border-radius: 10px;
  width: 46%;
  height:30%;
  margin-bottom:20px;
  margin-right:25px;
}

</style>
<div class="card_wrapper">
  @php
    $count = 1;
  @endphp 
  @foreach($pay_slips as $pay_slip)
  @php
    $count++;
    $employee = Employee::find($pay_slip->employee_id);
  @endphp
  @if($count == 7 || $count==13 || $count == 19 || $count == 25 || $count == 31 || $count ==37 || $count == 43 || $count == 49 || $count == 55 || $count == 61)
   <div class="card1">
      <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="width:50%;padding-left:10px;text-align:left">အမည်</td>
          <td style="text-align:left">{{$employee->name}}</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">လစာ</td>
          <td style="text-align:left">{{$employee->salary_rate}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">အချိန်ပို({{$pay_slip->total_ot}}-နာရီ)</td>
          <td style="text-align:left">{{round($pay_slip->ot_amount)}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">အချိန်ပို Bonus</td>
          <td style="text-align:left">{{round($pay_slip->ot_bonus_amount)}}ကျပ်</td>
        </tr>
        {{--<tr>
          <td style="width:50%;text-align:left;padding-left:10px;font-weight:bold">စုစုပေါင်း</td>
          <td style="text-align:left;font-weight:bold">{{ round($pay_slip->ot_amount + $pay_slip->salary_rate)}}ကျပ်</td>
        </tr>--}}
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ခွင့် ({{$pay_slip->total_leave}}-ရက်  {{$pay_slip->leave_time}}နာရီ)</td>
          <td style="text-align:left">{{round($pay_slip->leave_amount)}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ရက်မှန်ကြေး</td>
          <td style="text-align:left">{{$pay_slip->no_leave_bonus}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ကြိုငွေ</td>
          <td style="text-align:left">{{$pay_slip->loan}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px;font-weight:bold">စုစုပေါင်းလစာ</td>
          <td style="text-align:left;font-weight:bold">{{round($pay_slip->net_amount)}}ကျပ်</td>
        </tr>
      </table>
    </div>
    <div style="break-after:page"></div>
  @else
  <div class="card1">
      <table cellpadding="0" cellspacing="0" width="100%">
        <tr>
          <td style="width:50%;padding-left:10px;text-align:left">အမည်</td>
          <td style="text-align:left">{{$employee->name}}</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">လစာ</td>
          <td style="text-align:left">{{$employee->salary_rate}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">အချိန်ပို({{$pay_slip->total_ot}}-နာရီ)</td>
          <td style="text-align:left">{{round($pay_slip->ot_amount)}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">အချိန်ပို Bonus</td>
          <td style="text-align:left">{{round($pay_slip->ot_bonus_amount)}}ကျပ်</td>
        </tr>
        {{--<tr>
          <td style="width:50%;text-align:left;padding-left:10px;font-weight:bold">စုစုပေါင်း</td>
          <td style="text-align:left;font-weight:bold">{{ round($pay_slip->ot_amount + $pay_slip->salary_rate)}}ကျပ်</td>
        </tr>--}}
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ခွင့် ({{$pay_slip->total_leave}}-ရက် {{$pay_slip->leave_time}}နာရီ)</td>
          <td style="text-align:left">{{round($pay_slip->leave_amount)}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ရက်မှန်ကြေး</td>
          <td style="text-align:left">{{$pay_slip->no_leave_bonus}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px">ကြိုငွေ</td>
          <td style="text-align:left">{{$pay_slip->loan}}ကျပ်</td>
        </tr>
        <tr>
          <td style="width:50%;text-align:left;padding-left:10px;font-weight:bold">စုစုပေါင်းလစာ</td>
          <td style="text-align:left;font-weight:bold">{{number_format($pay_slip->net_amount,0)}}ကျပ်</td>
        </tr>
      </table>
  </div>
  @endif
  
  @endforeach
</div>