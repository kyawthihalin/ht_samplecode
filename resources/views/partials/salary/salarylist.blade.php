<?php
    use App\Models\Employee;
    $employees = Employee::where('stauts','Active')->get();
?>
@extends(backpack_view('blank'))
@push('after_styles')
<meta charset="utf-8">
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />
@endpush
@section('content')
<div class="card">
    <div class="card-body">
        <div class="row mb-3">
            <div class="col-md-3 d-none">
            <div class="form-group">
                    <select name="" class="form-control employee_name">
                        <option value="">-- Please Choose Employee --</option>
                        @foreach($employees as $emp)
                            <option value="{{$emp->id}}">{{$emp->name}}</option>
                        @endforeach
                       
                    </select>
                </div>
            </div>
            <div class="col-md-3 d-none">
                <div class="form-group">
                    <select name="" class="form-control select-month">
                        <option value="">-- Please Choose (Month) --</option>
                        <option value="01" @if(now()->format('m') == '01') selected @endif>Jan</option>
                        <option value="02" @if(now()->format('m') == '02') selected @endif>Feb</option>
                        <option value="03" @if(now()->format('m') == '03') selected @endif>Mar</option>
                        <option value="04" @if(now()->format('m') == '04') selected @endif>Apr</option>
                        <option value="05" @if(now()->format('m') == '05') selected @endif>May</option>
                        <option value="06" @if(now()->format('m') == '06') selected @endif>Jun</option>
                        <option value="07" @if(now()->format('m') == '07') selected @endif>Jul</option>
                        <option value="08" @if(now()->format('m') == '08') selected @endif>Aug</option>
                        <option value="09" @if(now()->format('m') == '09') selected @endif>Sep</option>
                        <option value="10" @if(now()->format('m') == '10') selected @endif>Oct</option>
                        <option value="11" @if(now()->format('m') == '11') selected @endif>Nov</option>
                        <option value="12" @if(now()->format('m') == '12') selected @endif>Dec</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3 d-none">
                <div class="form-group">
                    <select name="" class="form-control select-year">
                        <option value="">-- Please Choose (Year) --</option>
                        @for ($i = 0; $i < 5; $i++)
                        <option value="{{now()->subYears($i)->format('Y')}}" @if(now()->format('Y') == now()->subYears($i)->format('Y')) selected @endif>
                            {{now()->subYears($i)->format('Y')}}
                        </option>
                        @endfor
                    </select>
                </div>
            </div>
            <div class="col-md-10" id="error_message">
            </div>
            <div class="col-md-2">
            <button class="btn btn-success btn-sm btn-block search-btn"><i class="las la-check-square"></i>Approve Salary</button>
            </div>
           {{-- <div class="col-md-1">
                <a href="#" class="btn btn-success btn-sm btn-block" id="print"><i class="las la-print"></i>Print</a>
            </div> --}}
        </div>
        <div class="salary_overview_table" id="salary_overview_table"></div>
    </div>
</div>
@endsection
@push('after_scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />

<script>
    $(document).ready(function(){
        $('.search-btn').hide();
        toastr.options = {
            'closeButton': true,
            'debug': false,
            'newestOnTop': true,
            'progressBar': true,
            'positionClass': 'toast-top-center',
            'preventDuplicates': false,
            'showDuration': '1000',
            'hideDuration': '1000',
            'timeOut': '5000',
            'extendedTimeOut': '1000',
            'showEasing': 'swing',
            'hideEasing': 'linear',
            'showMethod': 'fadeIn',
            'hideMethod': 'fadeOut',
        }
        var token = '{{csrf_token()}}';
        $('.select-month').select2({
            placeholder: '-- Please Choose (Month) --',
            allowClear: true,
        });
        $('.select-year').select2({
            placeholder: '-- Please Choose (Year) --',
            allowClear: true,
        });
        $('.employee_name').select2({
            placeholder: '-- Please Choose (Employee) --',
            allowClear: true,
        });
      
       
        salaryOverviewTable();
        function salaryOverviewTable(){
            var employee_name = $('.employee_name').val();
            var month = $('.select-month').val();
            var year = $('.select-year').val();
            console.log(month);
            console.log(year);
            if(month == "" || year == "")
            {
                alert('လနှင့်နှစ်ကိုရွေးပါ');
            }
            else
            {
                $.ajax({
                    url: '{{route('salary_overview')}}',
                    type: 'POST',
                    async: false,
                    headers: {
                        'X-CSRF-Token': token
                    },
                    data: {
                        employee_name,
                        month,
                        year
                    },
                    success: function(res){
                        $('.salary_overview_table').html(res);
                        setTimeout($('.search-btn').show(), 10000);
                    }
                 });
            }
           
        }
        $('.search-btn').on('click', function(event){
            event.preventDefault();
            swal("Password ရိုက်ထည့်ပါ။", {
                content: {
                    element: "input",
                    attributes: {
                        type: "password",
                        autocomplete: "off",
                    },
                },
                icon: "warning",
                buttons: [
                    'မပြုလုပ်တော့ပါ။',
                    'လစာပေးခြင်းအတည်ပြုမည်။'
                ],
                dangerMode: true,
            }).then((value) => {
                if(!value)
                {
                    swal("မပြုလုပ်တော့ပါ။", "လစာပေးခြင်း ပယ်ဖျက်လိုက်ပါသည်။", "error");
                }else
                {
                    var  password= value;
                    var salaryDataArray = [];
                    $('.salary_data').each(function(i, obj) {
                        var leaveDay    = $(this).data('leaveday');
                        var totalOT     = $(this).data('totalot');
                        var perday      = $(this).data('perday');
                        var leaveAmount = $(this).data('leave');
                        var grandTotal  = $(this).data('grand');
                        var loan        = $(this).data('loan');
                        var total       = $(this).data('total');
                        var otAmount    = $(this).data('otamount');
                        var emp         = $(this).data('emp');
                        var salary_rate = $(this).data('salary_rate');
                        var no_leave_bonus = $(this).data('bn');
                        var leavetime = $(this).data('leavetime');
                        var ot_bonus_amount = $(this).data('otbonus');
                        var obj         = {'leaveDay':leaveDay,'Employee_id':emp,'Total_ot':totalOT,'perday':perday,'leaveAmount':leaveAmount,'grandTotal':grandTotal,'loan':loan,'total':total,'otAmount':otAmount,'salary_rate':salary_rate,'no_leave_bonus':no_leave_bonus,'leavetime':leavetime,'ot_bonus_amount':ot_bonus_amount};
                        salaryDataArray.push(obj);
                    });
                    $.ajax({
                        url: '{{route('salary_approve')}}',
                        type: 'POST',
                        async: false,
                        headers: {
                            'X-CSRF-Token': token
                        },
                        data: {
                            salaryDataArray,
                            password
                        },
                        success: function(res){
                            function myMessage() {
                                location.reload()
                            }
                            if(res.status == 200)
                            {
                                toastr.success('လစာစာရင်းပေးခြင်းအောင်မြင်ပါသည်။')
                                setTimeout(myMessage, 4000);
                            }
                        },
                        error:function(res)
                        {
                            function myMessage() {
                                location.reload()
                            }
                            if(res.responseJSON.error_code == 500)
                            {
                                toastr.error('လစာစာရင်းကိုပြန်လည်စစ်ဆေးပါ။')
                            }else if(res.responseJSON.error_code == "600")
                            {
                                toastr.error('Password မှားနေသည်။ပြန်လည်စစ်ဆေးပါ။')
                            }
                        }
                    
                    });
                }
            });
        });

        $('#print').on('click',function()
        {
            
            var printContent = document.getElementById("salary_overview_table");
            var htmlToPrint = '' +
            '<style type="text/css">' +
            'table th, table td {' +
            'border:1px solid #000;' +
            'padding:0.5em;' +
            '}' +
            'img {' +
                '-webkit-print-color-adjust: exact;'+
            '}'+
            '</style>';
            htmlToPrint += printContent.outerHTML;
            var WinPrint = window.open("");
            WinPrint.document.write(htmlToPrint);
         

            setTimeout(function(){
                WinPrint.print();
                WinPrint.close();
            },100);
            newWin.document.write('<html><body onload="window.print()" style="text-align:center;margin-top:50%">'+divToPrint.innerHTML+'</body></html>');

            newWin.document.close();

        })
    });
</script>
@endpush