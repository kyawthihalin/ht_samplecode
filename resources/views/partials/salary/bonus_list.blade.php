<div style="text-align: center">
    <button type="button" class="btn btn-app print-data">
        <i class="la la-print"></i>Print ထုတ်မည်
    </button>
    <button type="button" class="btn btn-app give-bonus">
        <i class="las la-piggy-bank"></i>Bonus ပေးမည်
    </button>

</div>
<br>
<div class="col-md-12 show-report" id="DivIdToPrint" style=" width: 100%; background-color: #ffffff; padding: 20px; -webkit-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
-moz-box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48);
box-shadow: 0px 6px 26px -6px rgba(0,0,0,0.48); border-radius: 5px;overflow: scroll;"></div>

@push('crud_fields_styles')
<style>
     li.active a {
            background-color: #222d32 !important;
        }

        .table-data {
            width: 100%;
        }

        .table-data, .table-data th, .table-data td {
            border-collapse: collapse;
            border: 1px solid #a8a8a8;
        }

        .table-data th {
            text-align: center;
            padding: 5px;
        }

        .table-data td {
            padding: 5px;
        }

        .table-data tbody > tr:nth-child(odd) {
            color: #606060;
        }
</style>
@endpush

@push('after_scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
@endpush

@push('crud_fields_scripts')
<script>
    var token = '{{csrf_token()}}';
    
    $(document).ready(function() {
        $('#saveActions').hide();
        $('small').hide();
        $("div").find(".col-md-8").addClass("col-md-12").remove("col-md-8");
        $.ajax({
            url: '{{route('greport')}}',
            type: 'GET',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            success: function(res){
                $('.show-report').html(res);
            },
           
        })

    })

    $('.give-bonus').on('click', function() {
        $.ajax({
            url: '{{route('giveBonus')}}',
            type: 'GET',
            async: false,
            headers: {
                'X-CSRF-Token': token
            },
            success: function(res){
                toastr.success('Bonus ပေးခြင်းအောင်မြင်ပါသည်။')
            },
           
        })
    });

    $('.print-data').on('click', function() {
        printDiv();
    });

    function printDiv() {

        var divToPrint = document.getElementById('DivIdToPrint');

        var newWin = window.open('', 'Print-Window');

        newWin.document.open();

        newWin.document.write('<html>' +
            '<head>' +
            '<style>\n' +
            '    .table-data\n' +
            '    {\n' +
            '        width:100%;\n' +
            '    }\n' +
            '\n' +
            '    .table-data,.table-data  th, .table-data  td\n' +
            '    {\n' +
            '        border-collapse:collapse;\n' +
            '        border: 1px solid #a8a8a8;\n' +
            '    }\n' +
            '\n' +
            '    .table-data  th\n' +
            '    {\n' +
            '        text-align: center;\n' +
            '        padding: 5px;\n' +
            '    }\n' +
            '\n' +
            '    .table-data  td\n' +
            '    {\n' +
            '        padding: 5px;\n' +
            '    }\n' +
            '\n' +

            '</style>' +
            '</head>' +
            '<body onload="window.print()">' + divToPrint.innerHTML + '</body>' +
            '</html>');

        newWin.document.close();

        setTimeout(function() {
            newWin.close();
        }, 10);

    }
</script>
@endpush