<?php
    use App\Helpers\getMonth;
?>
<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }


    .show-report {
        max-height: 640px;
        min-height: 640px;
        overflow: auto;
    }
</style>

@if(count($salaryList) > 0)
@php
    $monthName = getMonth::getmonthName($month);
@endphp
<h3 style="text-align:center">{{$monthName}} {{$year}} Salary</h3>
<table>
    <thead>
        <th>No</th>
        <th>Employee Name</th>
        <th>Salary Rate</th>
        <th>No Leave Bonus</th>
        <th>Salary Per Day</th>
        <th>Total Leave Day</th>
        <th>Leave Amount</th>
        <th>Total OT</th>
        <th>OT Extra Amount</th>
        <th>OT Amount</th>
        <th>OT Bonus</th>
        <th>Loan</th>
        <th>Pay Amount</th>
    </thead>

    <tbody>
    @php
        $count = 1;
        $salary_rate = 0;
        $total_ot = 0;
        $leave = 0;
        $pay = 0;
        $loan_amount = 0;
        $no_leave_amount = 0;
        $total_ot_bonus=0;
    @endphp
    @foreach($salaryList as $list)
    <tr>
        @php
            $salary_rate += $list->salary_rate;
            $total_ot += $list->ot_amount;
            $leave += $list->leave_amount;
            $pay += $list->net_amount;
            $loan_amount += $list->loan;
            $no_leave_amount += $list->no_leave_bonus;
            $total_ot_bonus += $list->ot_bonus_amount;
        @endphp
        <td>{{$count++}}</td>
        <td>{{$list->employee->name}}</td>
        <td>{{ number_format($list->salary_rate,0)}}</td>
        <td>{{number_format($list->no_leave_bonus,0)}}</td>
        <td>{{number_format($list->salary_per_day,4)}}</td>
        <td>{{$list->total_leave}}ရက် {{$list->leave_time}} နာရီ</td>
        <td>{{number_format($list->leave_amount,4)}}</td>
        <td>{{$list->total_ot}}</td>
        <td>{{$list->extra_ot_per_hour}}</td>
        <td>{{number_format($list->ot_amount,4)}}</td>
        <td>{{number_format($list->ot_bonus_amount,0)}}</td>
        <td>{{number_format($list->loan,4)}}</td>
        <td>{{number_format($list->net_amount,0)}}</td>

    </tr>
    @endforeach
    </tbody>

</table>
    <h5 style="text-align:center;font-weight:bold;margin-top:10px">Summary Table</h5>
<table>
    <thead>
        <th>No</th>
        <th>Description</th>
        <th>Amount</th>
    </thead>

    <tbody>
    <tr>
        <td>1</td>
        <td>Total Salary</td>
        <td>{{$salary_rate}}</td>
    </tr>
    <tr>
        <td>2</td>
        <td>Total OT</td>
        <td>{{number_format($total_ot,4)}}</td>
    </tr>
    <tr>
        <td>3</td>
        <td>Total OT Bonus</td>
        <td>{{number_format($total_ot_bonus,4)}}</td>
    </tr>
    <tr>
        <td>4</td>
        <td>Total Leave</td>
        <td>{{number_format($leave,4)}}</td>
    </tr>
    <tr>
        <td>5</td>
        <td>Total Loan</td>
        <td>{{number_format($loan_amount,0)}}</td>
    </tr>
    <tr>
        <td>6</td>
        <td>Total No Leave Bonus</td>
        <td>{{number_format($no_leave_amount,0)}}</td>
    </tr>
    <tr>
        <td>7</td>
        <td>Total Payable Amount</td>
        <td>{{number_format($pay,0)}}</td>
    </tr>
    </tbody>
</table>
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}"><h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif
