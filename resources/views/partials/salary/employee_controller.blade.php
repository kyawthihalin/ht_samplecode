@php
use Carbon\Carbon;
@endphp
<style>

    table {
        width: 100%;
    }

    table,th,td {
        border-collapse: collapse;
        border: 1px solid #a8a8a8;
    }

    th {
        text-align: center;
        padding: 5px;
    }

    td {
        padding: 5px;
    }

</style>
@if(count($employees) > 0)
<table>
    <thead>
        <th>စဉ်</th>
        <th>အမည်</th>
        <th>အလုပ်စဆင်းသည့်နေ့</th>
        <th>လုပ်သက်</th>
        <th>လစာ</th>
    </thead>
    
    <tbody>
    @php
        $count = 1;
        $total = 0;
    @endphp
    @foreach($employees as $emp)
    @php 
        $total += $emp->salary_rate;
        if($emp->joined_date != null || !$emp->joined_date != ""){
            $now = Carbon::now();
            $start_date =  Carbon::parse($emp->joined_date);
            $diff = $now->diff($start_date);
        }
    @endphp
    <tr>
        <td>{{$count++}}</td>
        <td>{{$emp->name}}</td>
        @if($emp->joined_date == null || $emp->joined_date == "")
            <td>-</td>
        @else
            <td>{{$emp->joined_date}}</td>
        @endif
        <td>
        {{$diff->y}} Years {{$diff->m}} Months {{$diff->d}} Days
        </td>
        <td>{{ number_format($emp->salary_rate,0)}}</td>
    </tr>
   
    @endforeach
    <tr style="font-weight:bold">
        <td colspan="3">Total Salary</td>
        <td>{{number_format($total,0)}}</td>
    </tr>
    </tbody>
  
</table>
@else
<img style="display: block;margin-left: auto;margin-right: auto;width: 30%;padding: 100px 100px 0 100px;" src="{{ asset('images/tenor.gif') }}"><h3 style="text-align: center;font-weight: 500;opacity: 0.4;text-shadow: 1px 1px;padding: 30px;font-size: 25px">No Data Found !!!</h3>
@endif