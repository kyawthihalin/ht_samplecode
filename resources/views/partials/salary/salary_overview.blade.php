
<?php
    use App\Models\Otextra;
    use App\Models\Salary;
?>
<style>

        table {
            width: 100%;
        }

        table, table th, table td {
            border-collapse: collapse;
            border: 1px solid #a8a8a8;
        }

        table th {
            text-align: center;
            padding: 5px;
        }

        table td {
            padding: 5px;
        }
</style>

<div class="table-responsive" id="attendance_table">
    <table class="table table-bordered table-striped">
        <thead>
            <th class="text-center bg-primary">စဉ်</th>
            <th class="text-center  bg-primary">အမည်</th>
            <th class="text-center  bg-primary">လစာ</th>
            <th class="text-center  bg-primary">ပျက်ရက်စုစုပေါင်း</th>
            <th class="text-center  bg-primary">ရက်မှန်ကြေး</th>
            <th class="text-center  bg-primary">အချိန်ပိုစုစုပေါင်း</th>
            <th class="text-center  bg-primary">တစ်ရက်စာလုပ်ခ</th>
            <th class="text-center  bg-primary">အချိန်ပိုပေါင်းငွေ</th>
            <th class="text-center  bg-primary">OT Bonus</th>
            <th class="text-center  bg-primary">ကြိုငွေ</th>
            <th class="text-center  bg-primary">ပျက်ရက်နှုတ်ခ</th>
            <th class="text-center  bg-primary">စုစုပေါင်း</th>
            <th class="text-center  bg-primary">ပေးငွေ</th>
        </thead>
        <tbody>
        @php
            $count = 1;
            $total_g = 0;
            $total_t = 0;
            $total_ot_amount = 0;
            $total_loan=0;
            $total_salary=0;
            $total_leave=0;
            $salary_exist_total=0;
            $salary_exist_loan =0;
            $salary_exist_ot = 0;
            $salary_exist_bonus = 0;
            $salary_exist_leave=0;
            $salary_exist_gtotal =0;
            $salary_exist_net=0;
            $salary_exist_ot_bonus = 0;
            $total_no_leave_bonus = 0;
            $total_ot_bonus = 0;
        @endphp
        @foreach ($employees as $employee)
            @php
                $total_ot = 0;
                $leave = 0;
                $office_day = 0;
                $salary = Salary::where('employee_id',$employee->id)->whereMonth('created_at',$month)->whereYear('created_at',$year)->first();
                $total_salary += $employee->salary_rate;
                $sunDay = 0;
                $bonusCount=0;
                $leavetime = 0;
            @endphp
            @if($salary)
            @php
                $salary_exist_loan += $salary->loan;
                $salary_exist_total += $salary->salary_rate;
                $salary_exist_ot_bonus += $salary->ot_bonus_amount;
                $salary_exist_ot += $salary->ot_amount;
                $salary_exist_leave += $salary->leave_amount;
                $salary_exist_gtotal += $salary->grand_total;
                $salary_exist_bonus += $salary->no_leave_bonus;
                $salary_exist_net += $salary->net_amount;
            @endphp
            <tr>
                <td>{{$count++}}</td>
                <td>{{$employee->name}}</td>
                <td>{{$salary->salary_rate}}</td>
                <td>{{$salary->total_leave}} ရက်{{$salary->leave_time}} နာရီ</td>
                <td>{{$salary->no_leave_bonus}}</td>
                <td>{{$salary->total_ot}} နာရီ</td>
                <td>{{$salary->salary_per_day}} </td>
                <td>{{$salary->ot_amount}}</td>
                <td>{{$salary->ot_bonus_amount}}</td>
                <td>{{$salary->loan}}</td>
                <td>{{$salary->leave_amount}}</td>
                <td>{{$salary->grand_total}}</td>
                <td>{{$salary->net_amount}}</td>
            </tr>
            @else

            <tr>
                <td>{{$count++}}</td>
                <td>{{$employee->name}}</td>
                <td>{{$employee->salary_rate}}</td>
                @foreach ($periods as $period)
                    @php
                        $office_day++;
                        $m_icon = '';
                        $e_icon = '';
                        $overtime = "-";
                        $attendance = collect($attendances)->where('employee_id', $employee->id)->where('attendence_date',
                        $period->format('Y-m-d'))->first();
                        if($attendance)
                        {

                            if($attendance->overtime == null)
                            {
                            }
                            else
                            {
                                $overtime = $attendance->overtime;
                                $total_ot += $attendance->overtime;
                            }
                            if($period->format('D') == 'Sun')
                            {
                                $sunDay = $sunDay + 1;
                            }
                            else
                            {
                                if($attendance->morning == 0 || $attendance->morning == null)
                                {
                                    $leave = $leave + 0.5;
                                }
                                if($attendance->evening == 0 || $attendance->evening == null)
                                {
                                    $leave = $leave + 0.5;
                                }
                                if($attendance->morning == 1 && $attendance->evening == 1)
                                {
                                    $worktime = $attendance->worktime;
                                    if($worktime < 8)
                                    {
                                        $leavetime += 8 - $worktime;
                                        $bonusCount++;
                                    }
                                }
                            }

                        }
                        else{
                            if($period->format('D') == 'Sun')
                            {
                                $sunDay = $sunDay + 1;
                            }
                            else
                            {
                                $leave = $leave + 1;
                            }
                        }
                    @endphp
                @endforeach
                @php
                    //amount per day of salary
                    $per_day = $employee->salary_rate / 30;
                    $onehourAmount = $per_day / 8;
                    $leavetimeAmount = $onehourAmount * $leavetime;
                    if($leave > 23)
                    {
                        $leave = $leave + 4;
                    }elseif($leave > 17)
                    {
                        $leave = $leave + 3;
                    }elseif($leave > 11)
                    {
                        $leave = $leave + 2;
                    }elseif($leave > 5)
                    {
                        $leave = $leave + 1;
                    }
                    //leave amount
                    $no_leave_bonus = 0;
                    if($leave == 0 && $bonusCount <= 1){
                        //change no leave bonus amount here
                        $no_leave_bonus = 10000;
                    }
                    $leave_amount = $per_day * $leave;

                    //ot amount
                    $ot = ($per_day/8)*$total_ot;
                    //get ot extra
                    $otextra = Otextra::orderBy('id','desc')->first();
                    $otEx_amount = $otextra->amount;
                    $extra = $total_ot * $otEx_amount;
                    $show_ot = $ot + $extra;
                    if($leave == $office_day)
                    {
                        $lAmount = $employee->salary_rate;
                    }
                    else
                    {
                        $lAmount = $leave_amount+$leavetimeAmount;
                    }
                    $grand_total = ($show_ot + $employee->salary_rate) - $lAmount;
                    $total = ceil($grand_total/100)*100;

                    $new_ot_bonus_amount = 0;
                    $ot_bonus = App\Models\OtBonus::get()->first();
                    if($ot_bonus){
                        $filteredData = array_filter($ot_bonus->bonuses, function ($ot_bonus) use ($total_ot) {
                            return $total_ot >= (int)$ot_bonus['at_least_hour'] && $total_ot <= (int)$ot_bonus['at_most_hour'];
                        });
                        if(!$filteredData){

                        }else
                        {
                            foreach($filteredData as $key=>$value){
                                $new_ot_bonus_amount = $filteredData[$key]['amount'];
                            }
                        }
                    }
                    $total_ot_bonus += $new_ot_bonus_amount;

                    $total_ot_amount += $show_ot;
                    $total_g += $grand_total + $no_leave_bonus + $new_ot_bonus_amount;
                    $total_t += $total+$no_leave_bonus + $new_ot_bonus_amount;
                    $total_leave += $lAmount;
                    $total_loan += $employee->per_month;
                    $total_no_leave_bonus += $no_leave_bonus;
                @endphp
                <td>
                    <div style="width:150px !important">
                        {{$leave}} ရက် {{$leavetime}}နာရီ
                    </div>
                </td>
                <td>
                    <div style="width:100px !important">
                        {{$no_leave_bonus}}
                    </div>
                </td>
                <td>
                    <div style="width:150px !important">
                        {{$total_ot}} နာရီ
                    </div>
                </td>
                <td>{{$per_day}} </td>

                <td>{{$show_ot}}</td>
                <td>{{$new_ot_bonus_amount}}</td>
                <td >
                    <div style="width:200px !important">
                        @if($employee->loan_amount < $employee->per_month)
                            <span class="text-danger" style="font-weight: bold;">{{$employee->per_month}}</span>
                            <small class="text-danger" style="font-weight: bold;">(ပြန်လည်စစ်ဆေးရန်)</small>
                        @else
                            <span>{{$employee->per_month}}</span>
                        @endif
                    </div>
                </td>
                <td>{{$lAmount}}</td>



                <td id="grand_total{{$employee->id}}">{{($grand_total + $no_leave_bonus + $new_ot_bonus_amount) - $employee->per_month}}</td>
                <td id="total{{$employee->id}}">
                    <div style="width:100px !important">
                        {{($total + $no_leave_bonus + $new_ot_bonus_amount) - $employee->per_month}}
                    </div>
                </td>
                <td  class="d-none salary_data" data-bn="{{$no_leave_bonus}}" data-emp="{{$employee->id}}" data-salary_rate="{{$employee->salary_rate}}" id="salary{{$employee->id}}" data-leaveDay="{{$leave}}" data-leavetime="{{$leavetime}}" data-totalOt="{{$total_ot}}" data-perday="{{$per_day}}" data-leave="{{$lAmount}}" data-grand="{{($grand_total + $no_leave_bonus + $new_ot_bonus_amount) - $employee->per_month}}" data-loan="{{$employee->per_month}}" data-total="{{($total  + $no_leave_bonus + $new_ot_bonus_amount) - $employee->per_month}}" data-otAmount="{{$show_ot}}" data-otbonus="{{$new_ot_bonus_amount}}"></td>
            </tr>
            @endif

            @endforeach

        </tbody>
    </table>
    @if($salary)
    <table>
        <thead>
            <th>NO</th>
            <th>Description</th>
            <th>Amount</th>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Total Salary</td>
                <td class="text-dark">{{$salary_exist_total}}</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Total OT Amount</td>
                <td class="text-dark"> {{$salary_exist_ot}}</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Total OT Bonus</td>
                <td class="text-dark"> {{$salary_exist_ot_bonus}}</td>
            </tr>
            <tr>
                <td>3</td>
                <td class="text-dark" style="font-weight:bold">Sub Total Amount</td>
                <td class="text-success" style="font-weight:bold">{{$salary_exist_total + $salary_exist_ot + $salary_exist_ot_bonus}}</td>
            </tr>
            <tr>
                <td>4</td>
                <td>Total Leave Amount</td>
                <td class="text-danger">{{$salary_exist_leave}}</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Total Loan Amount</td>
                <td class="text-danger">{{$salary_exist_loan}}</td>
            </tr>
            <tr>
                <td>6</td>
                <td>No Leave Bonus Amount</td>
                <td class="text-dark">{{$salary_exist_bonus}}</td>
            </tr>
            <tr>
                <td>7</td>
                <td class="text-dark" style="font-weight:bold">Grand Total</td>
                <td class="text-success" style="font-weight:bold" >{{$salary_exist_gtotal }}</td>
            </tr>
            <tr>
                <td>8</td>
                <td class="text-dark" style="font-weight:bold">Total</td>
                <td  class="text-success" style="font-weight:bold" >{{$salary_exist_net }}</td>
            </tr>
        </tbody>
    </table>
    @else

    <table>
        <thead>
            <th>NO</th>
            <th>Description</th>
            <th>Amount</th>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Total Salary</td>
                <td class="text-dark">{{$total_salary}}</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Total OT Amount</td>
                <td class="text-dark"> {{$total_ot_amount}}</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Total OT Bonus</td>
                <td class="text-dark"> {{$total_ot_bonus}}</td>
            </tr>
            <tr>
                <td>4</td>
                <td class="text-dark" style="font-weight:bold">Sub Total Amount</td>
                <td class="text-success" style="font-weight:bold">{{$total_ot_amount + $total_salary + $total_ot_bonus}}</td>
            </tr>
            <tr>
                <td>5</td>
                <td>Total Leave Amount</td>
                <td class="text-danger">{{$total_leave}}</td>
            </tr>
            <tr>
                <td>6</td>
                <td>Total Loan Amount</td>
                <td class="text-danger" id="show_loan">{{$total_loan}}</td>
            </tr>
            <tr>
                <td>7</td>
                <td>No Leave Bonus Amount</td>
                <td class="text-dark">{{$total_no_leave_bonus}}</td>
            </tr>
            <tr>
                <td>8</td>
                <td class="text-dark" style="font-weight:bold">Grand Total</td>
                <td class="text-success" style="font-weight:bold" id="gt" data-g="{{$total_g}}">{{$total_g - $total_loan}}</td>
            </tr>
            <tr>
                <td>9</td>
                <td class="text-dark" style="font-weight:bold">Total</td>
                <td  class="text-success" style="font-weight:bold" id="t" data-t="{{$total_t}}">{{$total_t - $total_loan}}</td>
            </tr>

        </tbody>
    </table>
    @endif
</div>

<script type="text/javascript">
    $(document).ready(function(){
        var check = <?php echo $salaryCount; ?>;
        var check_two = <?php echo $loan_exceed_count; ?>;
        if(check > 0)
        {
            $('.search-btn').hide();
        }
        if(check_two > 0)
        {
            $('#error_message').html("<div class='alert alert-info alert-dismissible fade show' role='alert'>လစာစာရင်းကိုပြန်လည်စစ်ဆေးပါ။<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>");
        }

    })


</script>
