@extends(backpack_view('blank'))
@section('content')
<script type="text/javascript">
  function display_c() {
    var refresh = 1000; // Refresh rate in milli seconds
    mytime = setTimeout('display_ct()', refresh)
  }

  function display_ct() {
    var x = new Date();
    var h = x.getHours(),
      m = x.getMinutes() + ":" + x.getSeconds();
    var ime = (h > 12) ? (h - 12 + ':' + m + ' PM') : (h + ':' + m + ' AM');
    document.getElementById('ct').innerHTML = ime;
    display_c();
  }
</script>
<section class="content-header" style="padding-top: 5px">
  <strong>
    <h1 class="container-fluid" style="font-family:Times New Roman; font-size:35px; font-weight: bold !important; color: #404e67;">
      Welcome Back, {{backpack_user()->name}} !
    </h1>
  </strong>
  <h3 class="container-fluid mb-5" style="padding-top: 0px;margin-top: 10px;">
    <small style="font-family: Centaur; font-size: 20px; font-weight: bold; color: #60099e;">
      Today is <?php
                $date = Carbon\Carbon::now();
                echo date('l, d F Y', strtotime($date)); //June, 2017
                ?>

      <body onload=display_ct();>
        <span id='ct'></span>
      </body>
    </small>
  </h3>
</section>
@endsection

@push('after_styles')
<link href="{{ asset('/css/app.css') }}" rel="stylesheet" />
@endpush
<?php
$widgets['after_content'][]=[
  'type' =>'div',
  'class'=>'container-fluid row pt-2 customwidth',
  'content'=>[
    [
      'type'       => 'card',
      'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
      'class'   => 'card text-white customcard1', // optional
      'content'    => [
        'body'   => '<p class="icon"><i class="las la-people-carry" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Active Employee</span></p><p class="total_amount">' . $employee_count .' ယောက်</p>
        <div class="view_detail"><a href="/admin/employee" style="color:white;font-weight:bold">View Details</a> </div>'
      ]
    ],
    [
        'type'       => 'card',
        'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
        'class'   => 'card text-white customcard2', // optional
        'content'    => [
          'body'   => '<p class="icon"><i class="las la-user-secret" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Total Customer</span></p><p class="total_amount">' . $supplier_count .' ယောက်</p>
          <div class="view_detail"><a href="/admin/supplier" style="color:white;font-weight:bold">View Details</a> </div>'
        ]
      ],
      [
        'type'       => 'card',
        'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
        'class'   => 'card text-white customcard4', // optional
        'content'    => [
          'body'   => '<p class="icon"><i class="las la-coins" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Monthly Income</span></p><p class="total_amount">' . $monthly_income .' ကျပ်</p>
          <div class="view_detail"><a href="/admin/ahtal-pop" style="color:white;font-weight:bold">View Details</a> </div>'
        ]
      ],
      [
        'type'       => 'card',
        'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
        'class'   => 'card text-white customcard3', // optional
        'content'    => [
          'body'   => '<p class="icon"><i class="las la-coins" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Monthly Income(Kpay)</span></p><p class="total_amount">' . $monthly_income_kpay .' ကျပ်</p>
          <div class="view_detail"><a href="/admin/ahtal-pop" style="color:white;font-weight:bold">View Details</a> </div>'
        ]
      ],
      [
        'type'       => 'card',
        'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
        'class'   => 'card text-white customcard1', // optional
        'content'    => [
          'body'   => '<p class="icon"><i class="las la-coins" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Daily Income</span></p><p class="total_amount">' . $daily_income .' ကျပ်</p>
          <div class="view_detail"><a href="/admin/ahtal-pop" style="color:white;font-weight:bold">View Details</a> </div>'
        ]
      ],
      [
        'type'       => 'card',
        'wrapper' => ['class' => 'col-sm-12 col-md-4'], // optional
        'class'   => 'card text-white customcard2', // optional
        'content'    => [
          'body'   => '<p class="icon"><i class="las la-coins" style="font-size:30px;padding-top:4px;"></i>&nbsp;&nbsp;&nbsp;<span class="title">Daily Income(Kpay)</span></p><p class="total_amount">' . $daily_income_kpay .' ကျပ်</p>
          <div class="view_detail"><a href="/admin/ahtal-pop" style="color:white;font-weight:bold">View Details</a> </div>'
        ]
      ],
  ]
]
?>

<?php
$widgets['after_content'][] = [
  'type' => 'div',
  'class' => 'container-fluid row pt-2',
  'content' => [ // widgets 
    [
        'type' => 'chart',
        'wrapperClass' => 'col-md-12',
        // 'class' => 'bg-white text-center',
        'controller' => \App\Http\Controllers\Admin\Charts\WeeklyIncomeChartController::class,
        'content' => [
          'body' => "နေ့စဥ်အထည်ပို့အကျဥ်းချုပ်".'<br>', // optional    
        ]
    ],
    [
        'type' => 'chart',
        'wrapperClass' => 'col-md-6',
        // 'class' => 'bg-white text-center',
        'controller' => \App\Http\Controllers\Admin\Charts\MonthlyIncomeChartController::class,
        'content' => [
          'body' => "လစဥ်အထည်ပို့၀င်ငွေအကျဥ်းချုပ်".'<br>', // optional    
        ]
      ],
      [
        'type' => 'chart',
        'wrapperClass' => 'col-md-6',
        // 'class' => 'bg-white text-center',
        'controller' => \App\Http\Controllers\Admin\Charts\MonthlyAhtalpopChartController::class,
        'content' => [
          'body' => "လစဥ်အထည်ပို့အရေအတွက်အကျဥ်းချုပ်".'<br>', // optional    
        ]
      ],
      
  ]
];
?>