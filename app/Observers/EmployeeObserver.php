<?php

namespace App\Observers;

use App\Models\Employee;
use App\Models\SalaryLog;

class EmployeeObserver
{
    /**
     * Handle the employee "created" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function created(Employee $employee)
    {
        //
    }

    /**
     * Handle the employee "updated" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function updated(Employee $employee)
    {
        //
    }

    public function updating(Employee $employee)
    {
      $emp = Employee::find($employee->id);
      if($emp->salary_rate != $employee->salary_rate)
      {
        $salary_log = new SalaryLog();
        $salary_log->employee_id = $employee->id;
        $salary_log->old_amount = $emp->salary_rate;
        $salary_log->increased_amount = $employee->salary_rate;
        $salary_log->salary_increased_date = $employee->updated_at;
        $salary_log->save();
      }

    }
    /**
     * Handle the employee "deleted" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function deleted(Employee $employee)
    {
        //
    }

    /**
     * Handle the employee "restored" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function restored(Employee $employee)
    {
        //
    }

    /**
     * Handle the employee "force deleted" event.
     *
     * @param  \App\Employee  $employee
     * @return void
     */
    public function forceDeleted(Employee $employee)
    {
        //
    }
}
