<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CashbookRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CashbookCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CashbookCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Cashbook::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/cashbook');
        CRUD::setEntityNameStrings('အကြွေးစာရင်း', 'အကြွေးစာရင်း');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'cashbook_date',
            'label'=> 'နေ့စွဲ',
            'type' => 'date'
        ]);
        CRUD::addColumn([
            'name' => 'status',
            'label'=> 'အကြွေးကျန်/အကြွေးဆပ်',
            'type' => 'closure',
            'function' => function($entry){
                if($entry->status == 'debt')
                {
                    return "အကြွေးကျန်";
                }
                elseif($entry->status == "cash")
                {
                    return "အကြွေးဆပ်";
                }
            }
        ]);
        CRUD::addColumn([
            'label'     => 'အမည်',
            'name'      => 'customer', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'customer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Supplier::class, // foreign key model
        ]);
        CRUD::addColumn([
            'name' => 'amount',
            'label'=> 'ကျသင့်ငွေ',
            'type' => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CashbookRequest::class);

        CRUD::addField([
            'name' => 'cashbook_date',
            'label'=> 'နေ့စွဲ',
            'type' => 'date'
        ]);
        CRUD::addField([
            'name' => 'status',
            'label'=> 'အကြွေးကျန်/အကြွေးဆပ်',
            'type'        => 'select2_from_array',
            'options'     => ['debt' => 'အကြွေးကျန်', 'cash' => 'အကြွေးဆပ်'],
            'allows_null' => false,
            'default'     => 'debt',
        ]);
        CRUD::addField([
            'label' => 'အမည်',
            'name' => 'customer_id',
            'type' => "relationship",
            'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'customer', // the method that defines the relationship in your Model
            'model' => "App\Models\Supplier", // foreign key Eloquent model
            'placeholder' => "အမည်ရွေးပါ", // placeholder for the select2 input
        ]);
        CRUD::addField([
            'name' => 'amount',
            'label'=> 'ကျသင့်ငွေ',
            'type' => 'number'
        ]);
        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
