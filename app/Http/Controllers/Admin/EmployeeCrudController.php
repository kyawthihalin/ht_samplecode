<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\EmployeeRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class EmployeeCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmployeeCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Employee::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/employee');
        CRUD::setEntityNameStrings('ဝန်ထမ်းစာရင်း', 'ဝန်ထမ်းစာရင်း');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess(['show']);
        CRUD::addClause('orderBy', 'name', 'asc');
        $this->crud->addFilter([
            'name'  => 'stauts',
            'type'  => 'dropdown',
            'label' => 'Status'
        ], [
            "Active" => 'Active',
            "Inactive" => 'Inactive',
        ], function ($value) { // if the filter is active
            $this->crud->addClause('where', 'stauts', $value);
        });
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'အမည်',
            'name' => 'name',
            'type' => 'text'
        ]);
        CRUD::addColumn([
            'label' => 'ဖုန်းနံပါတ်',
            'name' => 'phone',
            'type' => 'text'
        ]);
        CRUD::addColumn([
            'label' => 'NRC အရှေ့',
            'name' => 'nrc_front',
            'type' => 'image'
        ]);
        CRUD::addColumn([
            'label' => 'NRC အနောက်',
            'name' => 'nrc_back',
            'type' => 'image'
        ]);
        CRUD::addColumn([
            'label' => 'NRC နံပါတ်',
            'name' => 'nrc_number',
            'type' => 'text'
        ]);
        CRUD::addColumn([
            'label' => '၀န်ထမ်း ဓာတ်ပုံ',
            'name' => 'employee_photo',
            'type' => 'image'
        ]);
        CRUD::addColumn([
            'label' => 'လိပ်စာ',
            'name' => 'address',
            'type' => 'text',
            'limit' => 20000
        ]);
        CRUD::addColumn([
            'label' => 'လစာ',
            'name' => 'salary_rate',
            'type' => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'အလုပ်စဆင်းသည့်နေ့',
            'name' => 'joined_date',
            'type' => 'date'
        ]);
        CRUD::addColumn([
            'label' => 'လက်ရှိအလုပ်လုပ်ဆဲ',
            'name' => 'stauts',
            'type' => 'text'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
        $this->crud->disableResponsiveTable();
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(EmployeeRequest::class);
        CRUD::addField([
            'label' => 'အလုပ်စဆင်းသည့်နေ့',
            'name'  => 'joined_date',
            'type'  => 'date'
        ]);
        CRUD::addField([
            'name' => 'name',
            'type' => 'text',
            'label' => 'အမည်'
        ]);
        CRUD::addField([
            'name' => 'phone',
            'type' => 'number',
            'label' => 'ဖုန်းနံပါတ်'
        ]);
        CRUD::addField([
            'name' => 'address',
            'type' => 'textarea',
            'label' => 'လိပ်စာ'
        ]);

        CRUD::addField(
            [
                'name'  => 'nrc_front',
                'label' => 'မှတ်ပုံတင်ရှေ့ဘက်တင်ရန်',
                'type'  => 'browse'
            ]
        );

        CRUD::addField(
            [
                'name'  => 'nrc_back',
                'label' => 'မှတ်ပုံတင် အနောက်ခြမ်းတင်ရန်',
                'type'  => 'browse'
            ]
        );

        CRUD::addField(
            [
                'name'  => 'nrc_number',
                'label' => 'မှတ်ပုံတင် နံပါတ်',
                'type'  => 'text'
            ]
        );

        CRUD::addField(
            [
                'name'  => 'dob',
                'label' => 'မွေးနေ့',
                'type'  => 'date'
            ]
        );

        CRUD::addField(
            [
                'name'  => 'employee_photo',
                'label' => '၀န်ထမ်း ဓာတ်ပုံ',
                'type'  => 'browse'
            ]
        );

        CRUD::addField([
            'name' => 'salary_rate',
            'type' => 'number',
            'label' => 'လစာ'
        ]);
        CRUD::addField([
            'name' => 'stauts',
            'type' => 'select_from_array',
            'label' => 'လက်ရှိအလုပ်လုပ်ဆဲ',
            'options'     => ['Active' => 'Active', 'Inactive' => 'Inactive'],
            'allows_null' => false,
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
