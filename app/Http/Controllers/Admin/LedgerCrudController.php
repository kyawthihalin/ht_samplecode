<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LedgerRequest;
use App\Models\AhtalPop;
use App\Models\Expense;
use App\Models\HtalPop;
use App\Models\PateNyut;
use App\Models\PateWal;
use App\Models\StaffLoan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class LedgerCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class LedgerCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Ledger::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ledger');
        CRUD::setEntityNameStrings('စာရင်းချုပ်', 'စာရင်းချုပ်');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        // CRUD::setValidation(LedgerRequest::class);
 
        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                   // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 EDate'
                ]
            ],
        );
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/salary/ledger'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function report(Request $request){
        $startDate = $request->start_date;
        $endDate   = $request->end_date;
        $htal_pop  = AhtalPop::where('htal_pop_date','>=',$startDate)->where('htal_pop_date','<=',$endDate)->sum('get_amount');
        $htal_pop_kpay  = AhtalPop::where('htal_pop_date','>=',$startDate)->where('htal_pop_date','<=',$endDate)->sum('kpay');
        $pate_nyut = PateNyut::where('pate_nyut_date','>=',$startDate)->where('pate_nyut_date','<=',$endDate)->sum('total');
        $pate_wal  = PateWal::where('pate_wal_date','>=',$startDate)->where('pate_wal_date','<=',$endDate)->sum('total');
        $expenses  = Expense::where('expense_date','>=',$startDate)->where('expense_date','<=',$endDate)->selectRaw("expense_type_id")->selectRaw("SUM(cost) as total")->groupBy('expense_type_id')->get();;
        $loans  = StaffLoan::where('loan_date','>=',$startDate)->where('loan_date','<=',$endDate)->sum('loan_amount');
        return view('partials.salary.showledger', compact('htal_pop','pate_nyut','pate_wal','expenses','htal_pop_kpay','loans'))->render();
    }
}
