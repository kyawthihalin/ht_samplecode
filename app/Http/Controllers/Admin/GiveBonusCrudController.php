<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\GiveBonusRequest;
use App\Models\Bonus;
use App\Models\BonusEmployee;
use App\Models\Employee;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Carbon\Carbon;
/**
 * Class GiveBonusCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class GiveBonusCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\GiveBonus::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/give-bonus');
        CRUD::setEntityNameStrings('give bonus', 'give bonuses');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(GiveBonusRequest::class);

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/salary/bonus_list'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function give()
    {
        $employees = Employee::where('stauts','Active')->get();
        $bonuses = Bonus::first();
        foreach($employees as $emp)
        {
            $bonus = 0;
            if($emp->joined_date == null)
            {
                BonusEmployee::create([
                    'employee_id' => $emp->id,
                ]);
            }
            if($emp->joined_date != null || !$emp->joined_date != ""){
                $now = Carbon::now();
                $start_date =  Carbon::parse($emp->joined_date);
                $diff = $now->diff($start_date);
                $year = $diff->y;
                $month = $diff->m;
    
                if($year >= 1 && $month >= 6)
                {
                    $bonus_for_year = bcmul($year,$bonuses->bonus_per_year);
                    $bonus_for_month = bcdiv($bonuses->bonus_per_year,2);
                    $bonus = bcadd($bonus_for_year,$bonus_for_month);
                }elseif($year >=1 && $month < 6)
                {
                    $bonus = bcmul($year,$bonuses->bonus_per_year);
                }elseif($year < 1 && $month == 11)
                {
                    $bonus = $bonuses->eleven_month;
    
                }elseif($year < 1 && $month == 10)
                {
                    $bonus = $bonuses->ten_month;
    
                }elseif($year < 1 && $month > 6)
                {
                    $bonus = bcdiv($bonuses->bonus_per_year,2);
                }elseif($year < 1 && $month < 6)
                {
                    $bonus = $bonuses->bonus_under_year;
                }
                BonusEmployee::create([
                    'employee_id' => $emp->id,
                    'bonus_amount' => $bonus
                ]);
            }
                
        }
    }

    public function report()
    {
        
        $employees = Employee::where('stauts','Active')->get();
        $bonuses = Bonus::first();
        return view('partials.salary.bonus_controller', compact('employees','bonuses'))->render();
        
    }
}
