<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\OtBonusRequest;
use App\Models\OtBonus;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class OtBonusCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class OtBonusCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\OtBonus::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ot-bonus');
        CRUD::setEntityNameStrings('ot bonus', 'ot bonuses');
        CRUD::denyAccess(['show']);
        $row = OtBonus::all()->count();
        if ($row > 0) {
            CRUD::denyAccess(['create']);
        }
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'bonuses',
            'label' => 'Bonus List',
            'type' => 'closure',
            'function' => function ($entry) {
                $bonuses = $entry->bonuses;
                $count = 1;
                $table_header = "<div class='table-responsive'><table class='table'><thead><tr><th scope='col'>#</th><th scope='col'>အနည်းဆုံးရှိရမည့်နာရီ</th><th scope='col'>အများဆုံးရှိနိုင်သောနာရီ</th><th scope='col'>bonus ပေးမည့်ပမာဏ</th></tr></thead><tbody><tr>";
                $table_footer = "</tr></tbody></table></div>";
                $table_body = "";
                foreach ($bonuses as $bonus) {
                    $least_hour = $bonus['at_least_hour'];
                    $most_hour = $bonus['at_most_hour'];
                    $amount = $bonus['amount'];
                    $table_body .= "<th scope='row'>$count</th><td>$least_hour နာရီ</td><td>$most_hour နာရီ</td><td>$amount ကျပ်</td></tr><tr>";
                    $count++;
                }
                return $table_header . $table_body . $table_footer;
            }
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(OtBonusRequest::class);

        CRUD::addField(
            [   // repeatable
                'name'  => 'bonuses',
                'label' => 'Add OT Bonus',
                'type'  => 'repeatable',
                'fields' => [
                    [
                        'name'    => 'at_least_hour',
                        'type'    => 'number',
                        'label'   => 'အနည်းဆုံးရှိရမည့်နာရီ',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'at_most_hour',
                        'type'    => 'number',
                        'label'   => 'အများဆုံးရှိနိုင်သည့်နာရီ',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],
                    [
                        'name'    => 'amount',
                        'type'    => 'number',
                        'label'   => 'Bonus ပေးမည့်ပမာဏ',
                        'wrapper' => ['class' => 'form-group col-md-4'],
                    ],

                ],
                // optional
                'new_item_label'  => 'Add New OT Rule', // customize the text of the button
                'init_rows' => 1, // number of empty rows to be initialized, by default 1
            ],
        );

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
