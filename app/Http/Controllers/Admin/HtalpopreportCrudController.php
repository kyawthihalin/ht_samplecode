<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\HtalpopreportRequest;
use App\Models\AhtalPop;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;

/**
 * Class HtalpopreportCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class HtalpopreportCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Htalpopreport::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/htalpopreport');
        CRUD::setEntityNameStrings('htalpopreport', 'htalpopreports');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(HtalpopreportRequest::class);
        $this->crud->addField(
            [   // date_range
                'name'  => 'Sdate',
                'type'  => 'date_picker',
                'label' => 'Start Date',
                                // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 SDate'
                ]
            ],
        );
        $this->crud->addField(
            [   // date_range
                'name'  => 'Edate',
                'type'  => 'date_picker',
                'label' => 'End Date',
                   // optional:
                'date_picker_options' => [
                    'todayBtn' => 'linked',
                    'format'   => 'dd-mm-yyyy',
                    'language' => 'mm'
                ],
                'wrapperAttributes' => [
                    'class' => 'form-group col-md-6 EDate'
                ]
            ],
        );
        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/salary/htalpop'
        ]);
        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function report(Request $request){
        $startDate = $request->start_date;
        $endDate   = $request->end_date;
        $htal_pops  = AhtalPop::where('htal_pop_date','>=',$startDate)->where('htal_pop_date','<=',$endDate)->get();
        return view('partials.salary.showhtalpop', compact('htal_pops'))->render();
    }
}
