<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\ExpenseRequest;
use App\Models\Expense;
use App\Models\ExpenseType;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class ExpenseCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class ExpenseCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Expense::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/expense');
        CRUD::setEntityNameStrings('အထွက်စာရင်း', 'အထွက်စာရင်း');
        $this->crud->denyAccess('show');
        $this->crud->enableExportButtons();
            // daterange filter
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'ကြည့်ချင်သောနေ့စွဲအပိုင်းအခြားကိုရွေးပါ'
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'expense_date', '>=', $dates->from);
            $this->crud->addClause('where', 'expense_date', '<=', $dates->to);
        });
       
      // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'expense_type_id',
            'type'  => 'select2',
            'label' => 'Expense Type'
        ], function() {
            return [
                1 => 'အိမ် OT',
                2 => 'ကားဆီဖြည့်',
                3 => 'ကြိုငွေ',
                4 => 'အပြင်သုံး',
                5 => 'စက်',
            ];
        }, function($values) { // if the filter is active
            $this->crud->addClause('where', 'expense_type_id', $values);
        });

    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->disableResponsiveTable();
        CRUD::addColumn([
            'name' => 'expense_date',
            'type' => 'date',
            'lable'=> 'နေ့စွဲ'
        ]);
        CRUD::addColumn([
            'label'     => 'အမျိုးအစား',
            'name'      => 'type', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'type', // the method that defines the relationship in your Model
            'attribute' => 'type', // foreign key attribute that is shown to user
            'model'     => App\Models\ExpenseType::class, // foreign key model
        ]);
        CRUD::addColumn([
            'name' => 'description',
            'label'=> 'အကြောင်းအရာ',
            'type' => 'textarea',
        ]);
        CRUD::addColumn([
            'name' => 'cost',
            'label'=> 'ကုန်ကျစရိတ်',
            'type' => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(ExpenseRequest::class);

        CRUD::addField([
            'name' => 'expense_date',
            'type' => 'date',
            'lable'=> 'နေ့စွဲ'
        ]);
        CRUD::addField([
            'label' => 'အမျိုးအစား',
            'name' => 'expense_type_id',
            'type' => "relationship",
            'attribute' => "type", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'type', // the method that defines the relationship in your Model
            'model' => "App\Models\ExpenseType", // foreign key Eloquent model
            'placeholder' => "အမျိုးအစားရွေးပါ", // placeholder for the select2 input
        ]);
        CRUD::addField([
            'name' => 'description',
            'label'=> 'အကြောင်းအရာ',
            'type' => 'textarea',
        ]);
        CRUD::addField([
            'name' => 'cost',
            'label'=> 'ကုန်ကျစရိတ်',
            'type' => 'number'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
