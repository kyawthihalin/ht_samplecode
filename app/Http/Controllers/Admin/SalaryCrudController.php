<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SalaryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Attendence;
use App\Models\Employee;
use Carbon\CarbonPeriod;
use App\Models\Salary;
use App\Models\Otextra;
use Exception;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

/**
 * Class SalaryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SalaryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Salary::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/salary');
        CRUD::setEntityNameStrings('salary', 'salaries');
        CRUD::denyAccess(['create', 'show', 'delete']);
        CRUD::enableExportButtons();
        $month = date('m');
        $year = date('Y');
        CRUD::addClause('whereMonth', 'created_at', $month);
        CRUD::addClause('whereYear', 'created_at', $year);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::disableResponsiveTable();
        CRUD::addColumn([
            'label' => 'Employee Name',
            'type'  => 'closure',
            'function' => function ($entry) {
                $employee = Employee::find($entry->employee_id);
                return $employee->name;
            }
        ]);
        CRUD::column('salary_rate');
        CRUD::column('salary_per_day');
        CRUD::column('total_leave');
        CRUD::column('leave_amount');
        CRUD::column('total_ot');
        CRUD::column('ot_amount');
        CRUD::column('loan');
        CRUD::column('grand_total');
        CRUD::column('net_amount');
        CRUD::column('extra_ot_per_hour');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SalaryRequest::class);

        CRUD::field('loan');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function update(Request $request)
    {
        $old_salary = Salary::find($request->id);
        $old_loan   = $old_salary->loan;
        $new_loan   = (float)$request->loan;
        if ($new_loan == $old_loan) {
            \Alert::add('success', 'Successfully Updated Salary')->flash();
            return Redirect::to('admin/salary');
        } elseif ($new_loan > $old_loan) {
            $diff_amount = $new_loan - $old_loan;

            $old_salary->loan = $new_loan;

            $grand_total = $old_salary->grand_total - $diff_amount;

            $old_salary->grand_total = $grand_total;

            $total = ceil($grand_total / 100) * 100;

            $old_salary->net_amount  = $total;

            $old_salary->save();

            \Alert::add('success', 'Successfully Updated Salary')->flash();

            return Redirect::to('admin/salary');
        } elseif ($new_loan < $old_loan) {
            $diff_amount = $new_loan - $old_loan;

            $old_salary->loan = $new_loan;

            $grand_total = $old_salary->grand_total + $diff_amount;

            $old_salary->grand_total = $grand_total;

            $total = ceil($grand_total / 100) * 100;

            $old_salary->net_amount  = $total;

            $old_salary->save();

            \Alert::add('success', 'Successfully Updated Salary')->flash();

            return Redirect::to('admin/salary');
        }
    }

    public function paySlip(Request $request)
    {
        $date = $request->date;
        $employees = Employee::where('stauts', 'Active')->orderBy('name', 'asc')->get();
        return view('partials.salary.payslip_search', compact('employees', 'date'));
    }

    public function paySlipPrint(Request $request)
    {
        $month    = $request->month;
        $year     = $request->year;

        $pay_slips = Salary::whereMonth('created_at', $month)->whereYear('created_at', $year)->get();
        $employees = Employee::where('stauts', 'Active')->orderBy('name', 'asc')->get();

        return view('partials.salary.payslip', compact('pay_slips'))->render();
    }

    public function approveSalary(Request $request)
    {
        $password = $request->password;
        if (Hash::check($password, backpack_user()->password)){
            $salaryArray = $request->salaryDataArray;
            $extra = Otextra::orderBy('id', 'desc')->first();
            $extra_ot_per_hour = $extra->amount;
            DB::beginTransaction();
            foreach ($salaryArray as $sa) {
                $employee_id = $sa['Employee_id'];
                $salary_rate = $sa['salary_rate'];
                $total_leave = $sa['leaveDay'];
                $total_ot    = $sa['Total_ot'];
                $salary_per_day = $sa['perday'];
                $leave_amount   = $sa['leaveAmount'];
                $ot_amount      = $sa['otAmount'];
                $grand_total    = $sa['grandTotal'];
                $net_amount     = $sa['total'];
                $no_leave_bonus     = $sa['no_leave_bonus'];
                $ot_bonus_amount = $sa['ot_bonus_amount'];
                $leave_time = $sa['leavetime'];
                $employee = Employee::find($employee_id);
                $loan           = $employee->loan_amount;

                if ($employee->per_month > $loan) {
                    DB::rollBack();
                    return response()->json([
                        'error_code' => '500'
                    ], 500);
                } else {


                    $salary = new Salary();
                    $salary->employee_id = $employee_id;
                    $salary->salary_rate = $salary_rate;
                    $salary->total_leave = $total_leave;
                    $salary->total_ot    = $total_ot;
                    $salary->salary_per_day = $salary_per_day;
                    $salary->leave_amount = $leave_amount;
                    $salary->ot_amount = $ot_amount;
                    $salary->loan = $employee->per_month;
                    $salary->grand_total = $grand_total;
                    $salary->net_amount = $net_amount;
                    $salary->extra_ot_per_hour = $extra_ot_per_hour;
                    $salary->no_leave_bonus = $no_leave_bonus;
                    $salary->leave_time = $leave_time;
                    $salary->ot_bonus_amount = $ot_bonus_amount;
                    $salary->save();

                    $employee->loan_amount -= $employee->per_month;
                    $employee->per_month = 0;
                    $employee->save();
                }
            }
            DB::commit();
            return response()->json([
                'status' => '200'
            ], 200);
        }
        else
        {
            return response()->json([
                'error_code' => '600'
            ], 500);
        }
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
