<?php

namespace App\Http\Controllers\Admin\Charts;

use Backpack\CRUD\app\Http\Controllers\ChartController;
use ConsoleTVs\Charts\Classes\Chartjs\Chart;
use Illuminate\Support\Facades\DB;

/**
 * Class MonthlyAhtalpopChartController
 * @package App\Http\Controllers\Admin\Charts
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class MonthlyAhtalpopChartController extends ChartController
{
    public function setup()
    {
        $this->chart = new Chart();

        // MANDATORY. Set the labels for the dataset points
        $this->chart->labels([
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'Spetember',
            'October',
            'November',
            'December',
        ]);

        // RECOMMENDED. Set URL that the ChartJS library should call, to get its data using AJAX.
        $this->chart->load(backpack_url('charts/monthly-ahtalpop'));

        // OPTIONAL
        $this->chart->minimalist(false);
        $this->chart->displayLegend(true);
    }

    /**
     * Respond to AJAX calls with all the chart data points.
     *
     * @return json
     */
    public function data()
    {
        $year = date('Y');
        $transactions   = DB::table('ahtal_pops')->whereYear('htal_pop_date','=',$year)
                        ->select(DB::raw("date_format(htal_pop_date, '%m') as month,sum(quantity) as amount"))
                        ->groupBy('month')
                        ->get();
        $mountly_amount     = [];
        if (count($transactions) != 0) {
            foreach ($transactions as $transaction) {
                $month = ltrim($transaction->month, '0');
                $mountly_amount[$month-1] = $transaction->amount;
            }
        }
        for ($i = 0; $i < 12; $i++) {
            if (array_key_exists($i, $mountly_amount)) {
                $monthly_transfer_amount[$i] = $mountly_amount[$i];
            } else {
                $monthly_transfer_amount[$i] = 0;
            }
        }
        $this->chart->dataset("လစဥ်အထည်ပို့အကျဥ်းချုပ်", 'line', $monthly_transfer_amount)
            ->color('rgba(53, 78, 92)')
            ->backgroundColor('rgba(56, 151, 207)');
    }
}