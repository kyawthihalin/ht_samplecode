<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SuBooRequest;
use App\Models\FinancialAccount;
use App\Models\SuBoo;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

/**
 * Class SuBooCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SuBooCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SuBoo::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/su-boo');
        CRUD::setEntityNameStrings('su boo', 'su boos');
        $this->crud->denyAccess(['update', 'delete', 'show', 'clone']);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->removeAllButtonsFromStack('line');

        CRUD::addColumn([
            'label' => 'Account Type',
            'type'  => 'relationship',
            'name'  => 'financialAccount',
        ]);
        CRUD::addColumn([
            'label' => 'Amount',
            'type' => 'closure',
            'function' => function ($entry) {
                if ($entry->amount > 0) {
                    return "<p style='color:green;font-weight:bold'>$entry->amount</p>";
                } else {
                    return "<p style='color:red;font-weight:bold'>$entry->amount</p>";
                }
            }
        ]);
        CRUD::column('su_boo_date');
        CRUD::column('description');
        CRUD::column('created_at');
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SuBooRequest::class);

        CRUD::addField([
            'type' => "relationship",
            'name' => "financialAccount",
            'label' => "Financial Account",
            'placeholder' => "Please Select Su Boo Type"
        ]);
        CRUD::field('amount');
        CRUD::field('su_boo_date');
        CRUD::field('description');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(Request $request)
    {
        $financial_account = $request->financialAccount;
        $amount = $request->amount;
        $su_boo_date = $request->su_boo_date;
        $description = $request->description;

        $fin_acc = FinancialAccount::find($financial_account);
        if ($fin_acc) {
            if ($fin_acc->balance + $amount < 0) {
                \Alert::add('error', 'Failed! Please check your amount')->flash();
                return Redirect::to('admin/su-boo');
            } else {
                $fin_acc->balance += $amount;
                $fin_acc->save();
                $last_su_boo = SuBoo::orderBy('id', 'desc')->first();
                $new_su_boo = new SuBoo();
                $new_su_boo->financial_account_id = $financial_account;
                $new_su_boo->amount = $amount;
                $new_su_boo->su_boo_date = $su_boo_date;
                $new_su_boo->description = $description;
                if ($last_su_boo) {
                    $new_su_boo->closing_balance = $last_su_boo->closing_balance + $amount;
                } else {
                    $new_su_boo->closing_balance = $amount;
                }
                $new_su_boo->save();
                \Alert::add('success', 'Successfully Added to your SUBOO')->flash();
                return Redirect::to('admin/su-boo');
            }
        }
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
