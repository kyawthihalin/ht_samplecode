<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PateNyutRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class PateNyutCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PateNyutCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PateNyut::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/pate-nyut');
        CRUD::setEntityNameStrings('ပိတ်ညှပ်စာရင်း', 'ပိတ်ညှပ်စာရင်း');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('show');
            // daterange filter
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'ကြည့်ချင်သောနေ့စွဲအပိုင်းအခြားကိုရွေးပါ'
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'pate_nyut_date', '>=', $dates->from);
            $this->crud->addClause('where', 'pate_nyut_date', '<=', $dates->to);
        });

    }



    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'pate_nyut_date',
            'label' => 'နေ့စွဲ',
            'type' =>'date'
        ]);

        CRUD::addColumn([
            'name' => 'quantity',
            'label' => 'အရေအတွက်',
            'type' =>'number'
        ]);

        CRUD::addColumn([
            'name' => 'total',
            'label' => 'ကျသင့်ငွေ',
            'type' =>'number'
        ]);
        CRUD::addColumn([
            'name' => 'remark',
            'label' => 'မှတ်ချက်',
            'type' =>'text'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PateNyutRequest::class);

        CRUD::addField([
            'name' => 'pate_nyut_date',
            'label' => 'နေ့စွဲ',
            'type' =>'date'
        ]);
        CRUD::addField([
            'name' => 'quantity',
            'label' => 'အရေအတွက်',
            'type' =>'number'
        ]);
        CRUD::addField([
            'name' => 'total',
            'label' => 'ကျသင့်ငွေ',
            'type' =>'number'
        ]);
        CRUD::addField([
            'name' => 'remark',
            'label' => 'မှတ်ချက်',
            'type' =>'text'
        ]);
       

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
