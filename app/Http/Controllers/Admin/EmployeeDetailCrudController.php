<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\getMonth;
use App\Http\Requests\EmployeeDetailRequest;
use App\Models\Employee;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;


/**
 * Class EmployeeDetailCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class EmployeeDetailCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\EmployeeDetail::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/employee-detail');
        CRUD::setEntityNameStrings('employee detail', 'employee details');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        $employees = getMonth::getEmployee();

        // CRUD::setValidation(EmployeeDetailRequest::class);
        $this->crud->addField([
            'name'        => 'employee',
            'label'       => "Choose Employees",
            'type'        => 'select2_from_array',
            'options'     => $employees,
            'allows_multiple' => true,
        ]);

        $this->crud->addField([
            'name' => 'custom-ajax-button',
            'type' => 'view',
            'view' => 'partials/employees/detail'
        ]);


        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }

    public function report(Request $request)
    {
        if(!isset($request->employees))
        {
            $employees = Employee::where('stauts','Active')->get();
        }
        elseif($request->employees[0] == null)
        {
            $employees = Employee::where('stauts','Active')->get();
        }
        else{
            $employees = Employee::whereIn('id',$request->employees)->get();
        }
        return view('partials.employees.show', compact('employees'))->render();
    }
}
