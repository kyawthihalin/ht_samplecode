<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\CatalogueRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class CatalogueCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class CatalogueCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Catalogue::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/catalogue');
        CRUD::setEntityNameStrings('ဒီဇိုင်းများ', 'ဒီဇိုင်းများ');
        $this->crud->denyAccess('show');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'name' => 'code',
            'type' => 'text',
            'lable'=> 'ကုတ်နံပါတ်'
        ]);
        CRUD::addColumn([
            'name' => 'photo',
            'type' => 'image',
            'lable'=> 'ဓာတ်ပုံ'
        ]);

        CRUD::addColumn([
            'label'     => 'အမှတ်တံဆိပ်',
            'name'      => 'category', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'category', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Category::class, // foreign key model
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(CatalogueRequest::class);

        CRUD::addField(
            [
                'name'  => 'code',
                'label' => 'ကုတ်နံပါတ်ရိုက်ထည့်ပါ',
                'type'  => 'text'
            ]
        );
        CRUD::addField(
            [
                'name'  => 'photo',
                'label' => 'ဓာတ်ပုံတင်ရန်',
                'type'  => 'browse'
            ]
        );
        CRUD::addField(
            [   // relationship
                'type' => "relationship",
                'name' => 'category', // the method on your model that defines the relationship
                'label' => "အမှတ်တံဆိပ်ရွေးပါ",
                'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
                'entity' => 'category', // the method that defines the relationship in your Model
                'model' => "App\Models\Category", // foreign key Eloquent model
                'placeholder' => "အမှတ်တံဆိပ်ရွေးပါ", // placeholder for the select2 input
             ],
            );
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
