<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PateWalRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use App\Models\Shop;
/**
 * Class PateWalCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class PateWalCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\PateWal::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/pate-wal');
        CRUD::setEntityNameStrings('ပိတ်ဝယ်စာရင်း', 'ပိတ်ဝယ်စာရင်း');
        CRUD::addClause('orderBy','pate_wal_date','asc');
        $this->crud->enableExportButtons();
        $this->crud->denyAccess('show');
            // daterange filter
        $this->crud->addFilter([
            'type'  => 'date_range',
            'name'  => 'from_to',
            'label' => 'ကြည့်ချင်သောနေ့စွဲအပိုင်းအခြားကိုရွေးပါ'
        ],
        false,
        function ($value) { // if the filter is active, apply these constraints
            $dates = json_decode($value);
            $this->crud->addClause('where', 'pate_wal_date', '>=', $dates->from);
            $this->crud->addClause('where', 'pate_wal_date', '<=', $dates->to);
        });


          
        $this->crud->addFilter(
            [
                'name'  => 'shop_id',
                'type'  => 'dropdown',
                'label' => 'ဆိုင်အမည်ရွေးပါ',
            ],
            Shop::all()->pluck('name', 'id')->toArray(),
            function ($value) { // if the filter is active
                $this->crud->addClause('where', 'shop_id', '=', $value);
            }
        );
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::addColumn([
            'label' => 'နေ့စွဲ',
            'name' => 'pate_wal_date',
            'type' => 'date'
        ]);
        CRUD::addColumn([
            'label'     => 'ဆိုင်အမည်',
            'name'      => 'shop', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'shop', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Shop::class, // foreign key model
        ]);
        CRUD::addColumn([
            'label'     => 'အတိုင်းအတာ',
            'name'      => 'unit', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'unit', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Unit::class, // foreign key model
        ]);

        CRUD::addColumn([
            'label' => 'ပမာဏ',
            'name' => 'unit_amount',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'နှုန်း',
            'name' => 'unit_per_price',
            'type' => 'text'
        ]);

        CRUD::addColumn([
            'label' => 'ကျသင့်ငွေ',
            'name' => 'total',
            'type' => 'text'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name' => 'created_at',
            'type' => 'datetime'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name' => 'updated_at',
            'type' => 'datetime'
        ]);
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(PateWalRequest::class);

        CRUD::addField([
            'label' => 'နေ့စွဲ',
            'name' => 'pate_wal_date',
            'type' => 'date'
        ]);
        CRUD::addField([
            'label' => 'ဆိုင်အမည်',
            'name' => 'shop_id',
            'type' => "relationship",
            'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'shop', // the method that defines the relationship in your Model
            'model' => "App\Models\Shop", // foreign key Eloquent model
            'placeholder' => "ဆိုင်ရွေးပါ", // placeholder for the select2 input
        ]);
        CRUD::addField([
            'label' => 'အတိုင်းအတာ',
            'name' => 'unit_id',
            'type' => "relationship",
            'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'unit', // the method that defines the relationship in your Model
            'model' => "App\Models\Unit", // foreign key Eloquent model
            'placeholder' => "အတိုင်းအတာရွေးပါ", // placeholder for the select2 input
        ]);

        CRUD::addField([
            'label' => 'ပမာဏ',
            'name' => 'unit_amount',
            'type' => 'text'
        ]);

        CRUD::addField([
            'label' => 'နှုန်း',
            'name' => 'unit_per_price',
            'type' => 'text'
        ]);

        CRUD::addField([
            'label' => 'ကျသင့်ငွေ',
            'name' => 'total',
            'type' => 'text'
        ]);

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
