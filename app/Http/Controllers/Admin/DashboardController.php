<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\AhtalPop;
use App\Models\Employee;
use App\Models\Supplier;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(){
        $month = date('m');
        $year  = date('Y');
        $today   = date('d-m-Y');
        $employee_count = Employee::where('stauts','Active')->get()->count();
        $customer_count = Supplier::get()->count();
        $monthlyIncome  = AhtalPop::whereMonth('created_at',$month)->whereYear('created_at',$year)->sum('get_amount');
        $monthlyIncomekpay  = AhtalPop::whereMonth('created_at',$month)->whereYear('created_at',$year)->sum('kpay');
        $dailyIncome  = AhtalPop::where('created_at','=',$today)->sum('get_amount');
        $dailyIncomeKpay = AhtalPop::where('created_at','=',$today)->sum('kpay');
        return view(backpack_view('dashboard'))->with([
            'employee_count'  => $employee_count,
            'supplier_count'  => $customer_count,
            'monthly_income'  => $monthlyIncome,
            'monthly_income_kpay' => $monthlyIncomekpay,
            'daily_income' => $dailyIncome,
            'daily_income_kpay' => $dailyIncomeKpay
        ]);
    }
}
