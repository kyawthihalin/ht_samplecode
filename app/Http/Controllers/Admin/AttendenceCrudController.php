<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AttendenceRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Attendence;
use App\Models\Employee;
use Carbon\CarbonPeriod;

/**
 * Class AttendenceCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AttendenceCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Attendence::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/attendence');
        CRUD::setEntityNameStrings('attendence', 'attendences');
        $this->crud->setListView('partials.attendence.attendence_search');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('id');
        CRUD::column('employee_id');
        CRUD::column('morning');
        CRUD::column('evening');
        CRUD::column('attendence_date');
        CRUD::column('overtime');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AttendenceRequest::class);

        CRUD::field('id');
        CRUD::field('employee_id');
        CRUD::field('morning');
        CRUD::field('evening');
        CRUD::field('attendence_date');
        CRUD::field('overtime');
        CRUD::field('created_at');
        CRUD::field('updated_at');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function customCreate(Request $request)
    {
        $date = $request->date;
        $employees = Employee::where('stauts','Active')->orderBy('name','asc')->get();
        return view('partials.attendence.attendence',compact('employees','date'));
    }
    public function morningCheck(Request $request)
    {
        $today  = $request->date;
        $emp_id = $request->emp_id;
        $status = $request->status;
        $time   = $request->realtime;
        $attendence = Attendence::where('employee_id',$emp_id)->whereDate('attendence_date','=',$today)->first();
        if($attendence){
            $attendence->morning = $status;
            $attendence->worktime= $time;
            $attendence->save();
        }
        else{
            Attendence::create([
                'employee_id' => $emp_id,
                'morning' => $status,
                'evening' => 0,
                'worktime'=> $time,
                'attendence_date'=>$today,
                'created_at' => $today,
                'updated_at' => $today
            ]);
        }
        return response()->json([
            'status' => true,
            'message'=>'Success',
        ]);
    }

    public function eveningCheck(Request $request)
    {
        $today  = $request->date;
        $emp_id = $request->emp_id;
        $status = $request->status;
        $time   = $request->realtime;
        $attendence = Attendence::where('employee_id',$emp_id)->whereDate('attendence_date','=',$today)->first();
        if($attendence){
            $attendence->evening = $status;
            $attendence->worktime= $time;
            $attendence->save();
        }
        else{
            Attendence::create([
                'employee_id' => $emp_id,
                'morning' => 0,
                'evening' => $status,
                'worktime'=> $time,
                'attendence_date'=>$today,
                'created_at' => $today,
                'updated_at' => $today
            ]);
        }
      
        return response()->json([
            'status' => true,
            'message'=>'Success',
        ]);
    }

    public function overtime(Request $request)
    {
        $today  = $request->date;
        $emp_id = $request->emp_id;
        $overtime = $request->overtime;
        $attendence = Attendence::where('employee_id',$emp_id)->whereDate('attendence_date','=',$today)->first();
        if($attendence){
            $attendence->overtime = $overtime;
            $attendence->save();
        }
        else{
            Attendence::create([
                'employee_id' => $emp_id,
                'morning' => 0,
                'evening' => 0,
                'overtime' => $overtime,
                'attendence_date'=>$today,
                'created_at' => $today,
                'updated_at' => $today,
                'worktime' => 0
            ]);
        }
        return response()->json([
            'status' => true,
            'message'=>'Success',
        ]);
    }

    public function overview(Request $request)
    {
        $emp_id   = $request->employee_name;
        $month    = $request->month;
        $year     = $request->year;
        $attendances = "";

        $startOfMonth = $year . '-' . $month . '-01';
        $endOfMonth = Carbon::parse($startOfMonth)->endOfMonth()->format('Y-m-d');
        $periods = new CarbonPeriod($startOfMonth, $endOfMonth);

        if($emp_id == null || $emp_id == "")
        {
            $employees = Employee::where('stauts','Active')->orderBy('name','asc')->get();
            $attendances = Attendence::whereMonth('attendence_date', $month)->whereYear('attendence_date', $year)->get();
        }
        else
        {
            $employees   = Employee::where('id',$emp_id)->get();
            $attendances = Attendence::where('employee_id',$emp_id)->whereMonth('attendence_date', $month)->whereYear('attendence_date', $year)->get();
        }
        return view('partials.attendence.attendence_overview', compact('employees', 'periods', 'attendances'))->render();

    }

    public function workingtime(Request $request)
    {
        $today  = $request->date;
        $emp_id = $request->emp_id;
        $timeTotal = $request->timeTotal;
        $attendence = Attendence::where('employee_id',$emp_id)->whereDate('attendence_date','=',$today)->first();
        if($attendence){
            $attendence->worktime = $timeTotal;
            $attendence->save();
        }
        else{
            Attendence::create([
                'employee_id' => $emp_id,
                'morning' => 0,
                'evening' => 0,
                'overtime' => 0,
                'worktime' => $timeTotal,
                'attendence_date'=>$today,
                'created_at' => $today,
                'updated_at' => $today
            ]);
        }
        return response()->json([
            'status' => true,
            'message'=>'Success',
        ]);
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
