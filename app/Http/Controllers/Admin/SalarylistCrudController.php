<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\SalarylistRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Attendence;
use App\Models\Employee;
use Carbon\CarbonPeriod;
use App\Models\Salary;

/**
 * Class SalarylistCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SalarylistCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\Salarylist::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/salarylist');
        CRUD::setEntityNameStrings('salarylist', 'salarylists');
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */

    public function customList(Request $request)
    {
        $date = $request->date;
        $employees = Employee::where('stauts','Active')->orderBy('name','asc')->get();
        return view('partials.salary.salarylist',compact('employees','date'));
    }

    public function overview(Request $request)
    {
        $emp_id   = $request->employee_name;
        $month    = $request->month;
        $year     = $request->year;
        $loan_exceed_count = 0;
        $attendances = "";
        $startOfMonth = $year . '-' . $month . '-01';
        $endOfMonth = Carbon::parse($startOfMonth)->endOfMonth()->format('Y-m-d');
        $periods = new CarbonPeriod($startOfMonth, $endOfMonth);
        $salaryCount = Salary::whereMonth('created_at',$month)->whereYear('created_at',$year)->get()->count();
        $emps = Employee::where('loan_amount','>',0)->orWhere('per_month','>',0)->get();
        if($emps)
        {
            foreach($emps as $em)
            {
                if($em->loan_amount < $em->per_month)
                {
                    $loan_exceed_count++;
                }
            }
        }
        if($emp_id == null || $emp_id == "")
        {
            $employees = Employee::where('stauts','Active')->orderBy('name','asc')->get();
            $attendances = Attendence::whereMonth('attendence_date', $month)->whereYear('attendence_date', $year)->get();
        }
        else
        {
            $employees   = Employee::where('id',$emp_id)->get();
            $attendances = Attendence::where('employee_id',$emp_id)->whereMonth('attendence_date', $month)->whereYear('attendence_date', $year)->get();
        }
        return view('partials.salary.salary_overview', compact('employees', 'periods', 'attendances','year','month','salaryCount','loan_exceed_count'))->render();

    }

    protected function setupCreateOperation()
    {
        CRUD::setValidation(SalarylistRequest::class);

        

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
