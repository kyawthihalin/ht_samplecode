<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\StaffLoanRequest;
use App\Models\Employee;
use App\Models\StaffLoan;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;
use Prologue\Alerts\Facades\Alert;

/**
 * Class StaffLoanCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class StaffLoanCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\StaffLoan::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/staff-loan');
        CRUD::setEntityNameStrings('ကြိုငွေ', 'ကြိုငွေထည့်ရန်');
        $this->crud->denyAccess(['show']);
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        CRUD::column('loan_amount');
        CRUD::column('employee_id');
        CRUD::column('loan_date');
        CRUD::column('created_at');
        CRUD::column('updated_at');

        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(StaffLoanRequest::class);
        CRUD::field('loan_date');
        CRUD::field('employee_id');
        CRUD::field('loan_amount');

        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    public function store(StaffLoanRequest $request){
        StaffLoan::create([
            'loan_date' => $request->loan_date,
            'employee_id' => $request->employee_id,
            'loan_amount' => $request->loan_amount
        ]);
        $emp = Employee::find($request->employee_id);
        $emp->loan_amount += $request->loan_amount;
        $emp->save();
        \Alert::success('success',"Successfully Added Loan Amount");
        return redirect('admin/staff-loan');
    }

    public function update(StaffLoanRequest $request)
    {
        $transaction = StaffLoan::find($request->id);
        $transaction_amount = $transaction->loan_amount;
        $loan_amount = $request->loan_amount;
        $emp = Employee::find($transaction->employee_id);
        if($loan_amount > 0)
        {
            if($transaction_amount > $loan_amount)
            {
                $emp->loan_amount -= $transaction_amount - $loan_amount;

            }
            else{
                $emp->loan_amount += $loan_amount - $transaction_amount;
            }
            $emp->save();
        }
        elseif($loan_amount == 0)
        {
            $emp->loan_amount -= $transaction_amount;
            $emp->save();
        }
        $transaction->loan_amount = $request->loan_amount;
        $transaction->save();
        \Alert::success('success',"Successfully Updated Loan Amount");
        return redirect('admin/staff-loan');
    }

    public function destroy($id)
    {
        $transaction = StaffLoan::find($id);
        $transaction_amount = $transaction->loan_amount;
        $employee = $transaction->employee_id;
        $emp = Employee::find($employee);
        $emp->loan_amount -= $transaction_amount;
        if($emp->loan_amount == 0)
        {
            $emp->per_month = 0;
        }
        $emp->save();
        return $this->crud->delete($id);
    }
    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        // $this->setupCreateOperation();
        CRUD::setValidation(StaffLoanRequest::class);
        CRUD::addField([
            'name' => 'employee',
            'type' => 'relationship',
            'label' => 'Employee Name',
            'attributes' => [
                'readonly'    => 'readonly',
                'disabled'    => 'disabled',
            ], 
        ]);
        CRUD::addField([
            'name' => 'loan_date',
            'label' => 'Loan Date',
            'type' => 'date',
        ]);
        
        CRUD::field('loan_amount');
    }
}
