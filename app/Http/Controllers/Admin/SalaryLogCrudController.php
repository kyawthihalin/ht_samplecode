<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\getMonth;
use App\Http\Requests\SalaryLogRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class SalaryLogCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SalaryLogCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\SalaryLog::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/salary-log');
        CRUD::setEntityNameStrings('salary log', 'salary logs');
        CRUD::denyAccess(['create', 'delete', 'show', 'update']);
        $this->crud->addFilter(
            [
                'type'  => 'date_range',
                'name'  => 'from_to',
                'label' => 'ကြည့်ချင်သောနေ့စွဲအပိုင်းအခြားကိုရွေးပါ'
            ],
            false,
            function ($value) { // if the filter is active, apply these constraints
                $dates = json_decode($value);
                $this->crud->addClause('where', 'salary_increased_date', '>=', $dates->from);
                $this->crud->addClause('where', 'salary_increased_date', '<=', $dates->to);
            }
        );

        // select2_multiple filter
        $this->crud->addFilter([
            'name'  => 'employee',
            'type'  => 'select2_multiple',
            'label' => 'Choose Employee Name'
        ], function () {
            return getMonth::getEmployee();
        }, function ($values) { // if the filter is active
            $this->crud->addClause('whereIn', 'employee_id', json_decode($values));
        });

        $this->crud->enableExportButtons();
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
        CRUD::addColumn([
            'label'     => '၀န်ထမ်းအမည်',
            'name'      => 'name', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'employee', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Employee::class, // foreign key model
        ]);

        CRUD::addColumn([
            'label' => 'မူလ လစာ',
            'name' => 'old_amount',
            'type' => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'တိုး/လျော့သော လစာ',
            'name' => 'increased_amount',
            'type' => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'လစာတိုး/လျော့သောနေ့စွဲ',
            'name' => 'salary_increased_date',
            'type' => 'date'
        ]);

        $this->crud->removeAllButtonsFromStack('line');
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SalaryLogRequest::class);



        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
