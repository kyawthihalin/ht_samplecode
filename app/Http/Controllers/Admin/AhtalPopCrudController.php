<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AhtalPopRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class AhtalPopCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class AhtalPopCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     * 
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\App\Models\AhtalPop::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/ahtal-pop');
        CRUD::setEntityNameStrings('ahtal pop', 'ahtal pops');
        $this->crud->enableExportButtons();
    }

    /**
     * Define what happens when the List operation is loaded.
     * 
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        $this->crud->disableResponsiveTable();

        CRUD::addColumn([
            'label' => 'အထည့်ပို့နေ့စွဲ',
            'name'  => 'htal_pop_date',
            'type'  => 'date'
        ]);
        CRUD::addColumn([
            'label' => 'Code Number',
            'name'  => 'code',
            'type'  => 'text'
        ]);
        CRUD::addColumn([
            'label'     => 'တံဆိပ်အမည်',
            'name'      => 'tag', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'tag', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Category::class, // foreign key model
        ]);
        CRUD::addColumn([
            'label'     => '၀ယ်သူအမည်',
            'name'      => 'customer', // name of relationship method in the model
            'type'      => 'relationship',
            'entity'    => 'customer', // the method that defines the relationship in your Model
            'attribute' => 'name', // foreign key attribute that is shown to user
            'model'     => App\Models\Supplier::class, // foreign key model
        ]);      
        CRUD::addColumn([
            'label' => 'အ‌ရေအတွက်',
            'name'  => 'quantity',
            'type'  => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'ကျသင့်ငွေ',
            'name'  => 'total',
            'type'  => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'ကြွေးကျန်ရရှိ',
            'name'  => 'get_amount',
            'type'  => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'ကြွေးကျန်',
            'name'  => 'rest_amount',
            'type'  => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'KPay',
            'name'  => 'kpay',
            'type'  => 'number'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းထည့်သည့်နေ့',
            'name'  => 'created_at',
            'type'  => 'date'
        ]);
        CRUD::addColumn([
            'label' => 'စာရင်းပြင်သည့်နေ့',
            'name'  => 'updated_at',
            'type'  => 'date'
        ]);


        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']); 
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(AhtalPopRequest::class);
        CRUD::addField([
            'label' => 'နေ့စွဲ',
            'name'  => 'htal_pop_date',
            'type'  => 'date'
        ]);
        CRUD::addField([
            'label' => 'Code Number',
            'name'  => 'code',
            'type'  => 'text'
        ]);        
        CRUD::addField([
            'label' => 'တံဆိပ်အမည်',
            'name' => 'category_id',
            'type' => "relationship",
            'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'tag', // the method that defines the relationship in your Model
            'model' => "App\Models\Category", // foreign key Eloquent model
            'placeholder' => "တံဆိပ်အမည်", // placeholder for the select2 input
        ]);
        CRUD::addField([
            'label' => '၀ယ်သူအမည်',
            'name' => 'supplier_id',
            'type' => "relationship",
            'attribute' => "name", // foreign key attribute that is shown to user (identifiable attribute)
            'entity' => 'customer', // the method that defines the relationship in your Model
            'model' => "App\Models\Supplier", // foreign key Eloquent model
            'placeholder' => "၀ယ်သူအမည်", // placeholder for the select2 input
        ]);

        CRUD::addField([
            'label' => 'အရေအတွက်',
            'name'  => 'quantity',
            'type'  => 'number'
        ]);
        CRUD::addField([
            'label' => 'ကျသင့်ငွေ',
            'name'  => 'total',
            'type'  => 'number'
        ]);
        CRUD::addField([
            'label' => 'ကြွေးကျန်ရရှိ',
            'name'  => 'get_amount',
            'type'  => 'number'
        ]);
        CRUD::addField([
            'label' => 'ကြွေးကျန်',
            'name'  => 'rest_amount',
            'type'  => 'number'
        ]);
        CRUD::addField([
            'label' => 'Kpay ရငွေ',
            'name'  => 'kpay',
            'type'  => 'number'
        ]);
        
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number'])); 
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     * 
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
