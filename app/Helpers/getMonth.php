<?php

namespace App\Helpers;

use App\Models\Employee;

class getMonth
{
    public static function getmonthName($month){
        $monthName = "";
        if($month == "1")
        {
            $monthName = "January";
            return $monthName;
        }
        elseif($month == "2")
        {
            $monthName = "February";
            return $monthName;
        }
        elseif($month == "3")
        {
            $monthName = "March";
            return $monthName;
        }
        elseif($month == "4")
        {
            $monthName = "April";
            return $monthName;
        }
        elseif($month == "5")
        {
            $monthName = "May";
            return $monthName;
        }
        elseif($month == "6")
        {
            $monthName = "June";
            return $monthName;
        }
        elseif($month == "7")
        {
            $monthName = "July";
            return $monthName;
        }
        elseif($month == "8")
        {
            $monthName = "August";
            return $monthName;
        }
        elseif($month == "9")
        {
            $monthName = "September";
            return $monthName;
        }
        elseif($month == "10")
        {
            $monthName = "October";
            return $monthName;
        }
        elseif($month == "11")
        {
            $monthName = "November";
            return $monthName;
        }
        elseif($month == "12")
        {
            $monthName = "December";
            return $monthName;
        }
    }

    public static function getEmployee()
    {
        $employees = Employee::get();
        $data = [];
        foreach($employees as $emp){
            $data[$emp->id] = $emp->name;
        }
        return  $data;        
    }
}