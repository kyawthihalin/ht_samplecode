<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddInfosToEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->text('nrc_front')->nullable();
            $table->text('nrc_back')->nullable();
            $table->string('nrc_number')->nullable();
            $table->date('dob')->nullable();
            $table->text('employee_photo')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employees', function (Blueprint $table) {
            $table->dropColumn('nrc_front');
            $table->dropColumn('nrc_back');
            $table->dropColumn('nrc_number');
            $table->dropColumn('dob');
            $table->dropColumn('employee_photo');
        });
    }
}
