<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAhtalPopsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ahtal_pops', function (Blueprint $table) {
            $table->id();
            $table->string('code')->nullable();
            $table->date('htal_pop_date')->nullable();
            $table->unsignedBigInteger('customer_id')->nullable();
            $table->unsignedBigInteger('category_id')->nullable();
            $table->bigInteger('quantity')->nullable();
            $table->double('get_amount')->default(0);
            $table->double('rest_amount')->default(0);
            $table->double('kpay')->default(0);
            $table->double('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ahtal_pops');
    }
}
