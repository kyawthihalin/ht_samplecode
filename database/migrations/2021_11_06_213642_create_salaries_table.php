<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('employee_id')->nullable();
            $table->double('salary_rate')->default(0);
            $table->double('total_leave')->default(0);
            $table->double('total_ot')->default(0);
            $table->double('salary_per_day')->default(0);
            $table->double('leave_amount')->default(0);
            $table->double('ot_amount')->default(0);
            $table->double('loan')->default(0);
            $table->double('grand_total')->default(0);
            $table->double('net_amount')->default(0);
            $table->double('extra_ot_per_hour')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
