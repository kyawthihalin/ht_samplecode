<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSuBooTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('su_boos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('financial_account_id')->nullable();
            $table->double("amount")->nullable();
            $table->date("su_boo_date")->nullable();
            $table->text("description")->nullable();
            $table->double("closing_balance")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('su_boos');
    }
}
