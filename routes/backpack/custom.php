<?php

// --------------------------
// Custom Backpack Routes
// --------------------------
// This route file is loaded automatically by Backpack\Base.
// Routes you generate using Backpack\Generators will be placed here.

Route::group([
    'prefix'     => config('backpack.base.route_prefix', 'admin'),
    'middleware' => array_merge(
        (array) config('backpack.base.web_middleware', 'web'),
        (array) config('backpack.base.middleware_key', 'admin')
    ),
    'namespace'  => 'App\Http\Controllers\Admin',
], function () { // custom admin routes
    Route::crud('category', 'CategoryCrudController');
    Route::crud('catalogue', 'CatalogueCrudController');
    Route::crud('supplier', 'SupplierCrudController');
    Route::crud('employee', 'EmployeeCrudController');
    Route::crud('pate-nyut', 'PateNyutCrudController');
    Route::crud('shop', 'ShopCrudController');
    Route::crud('unit', 'UnitCrudController');
    Route::crud('pate-wal', 'PateWalCrudController');
    Route::crud('expense', 'ExpenseCrudController');
    Route::crud('cashbook', 'CashbookCrudController');
    Route::crud('htal-pop', 'HtalPopCrudController');
    Route::crud('attendence', 'AttendenceCrudController');
    Route::post('morning','AttendenceCrudController@morningCheck')->name('morning');
    Route::post('evening','AttendenceCrudController@eveningCheck')->name('evening');
    Route::post('overtime','AttendenceCrudController@overtime')->name('overtime');
    Route::post('workingtime','AttendenceCrudController@workingtime')->name('workingtime');
    Route::get('attendence/list','AttendenceCrudController@customCreate')->name('attendence');
    Route::get('salarylist/list','SalarylistCrudController@customList')->name('salarylist');
    Route::get('salary/getList','SalaryCrudController@paySlip')->name('paySlip');
    Route::post('salary/payslip','SalaryCrudController@paySlipPrint')->name('payslip_print');

    Route::post('salary/approve','SalaryCrudController@approveSalary')->name('salary_approve');
    Route::post('salary/report','SalaryreportCrudController@report')->name('report');
    Route::post('ledger/report','LedgerCrudController@report')->name('Lreport');

    Route::get('employee/report','EmployeereportCrudController@report')->name('ereport');
    Route::get('give_bonus/report','GiveBonusCrudController@report')->name('greport');
    Route::get('give_bonus','GiveBonusCrudController@give')->name('giveBonus');

    Route::post('leave/list','LeaveListCrudController@report')->name('lreport');
    

    Route::post('attendance-overview-table','AttendenceCrudController@overview')->name('attendence_overview');
    Route::post('salary-overview-table','SalarylistCrudController@overview')->name('salary_overview');


    Route::post('htalpopreport/getreport','HtalpopreportCrudController@report')->name('htalpopreport');
    


    Route::crud('salarylist', 'SalarylistCrudController');
    Route::crud('otextra', 'OtextraCrudController');
    Route::crud('salary', 'SalaryCrudController');
    Route::crud('salaryreport', 'SalaryreportCrudController');
    Route::crud('employeereport', 'EmployeereportCrudController');
    Route::crud('expense-type', 'ExpenseTypeCrudController');
    Route::crud('ahtal-pop', 'AhtalPopCrudController');
    Route::crud('ledger', 'LedgerCrudController');
    Route::crud('htalpopreport', 'HtalpopreportCrudController');
    Route::get('dashboard','DashboardController@index');
    Route::get('charts/weekly-ahtalpop', 'Charts\WeeklyAhtalpopChartController@response')->name('charts.weekly-ahtalpop.index');
    Route::get('charts/monthly-ahtalpop', 'Charts\MonthlyAhtalpopChartController@response')->name('charts.monthly-ahtalpop.index');
    Route::get('charts/weekly-income', 'Charts\WeeklyIncomeChartController@response')->name('charts.weekly-income.index');
    Route::get('charts/monthly-income', 'Charts\MonthlyIncomeChartController@response')->name('charts.monthly-income.index');
    Route::get('charts/weekly-income-kpay', 'Charts\WeeklyIncomeKpayChartController@response')->name('charts.weekly-income-kpay.index');
    Route::get('charts/monthly-income-kpay', 'Charts\MonthlyIncomeKpayChartController@response')->name('charts.monthly-income-kpay.index');
    Route::crud('staff-loan', 'StaffLoanCrudController');
    Route::crud('paystaffloan', 'PaystaffloanCrudController');
    Route::crud('employee-detail', 'EmployeeDetailCrudController');
    Route::post('employee-detail/report','EmployeeDetailCrudController@report')->name('empreport');

    Route::crud('salary-log', 'SalaryLogCrudController');
    Route::crud('financial-account', 'FinancialAccountCrudController');
    Route::crud('su-boo', 'SuBooCrudController');
    Route::crud('bonus', 'BonusCrudController');
    Route::crud('bonus-employee', 'BonusEmployeeCrudController');
    Route::crud('give-bonus', 'GiveBonusCrudController');
    Route::crud('leave-list', 'LeaveListCrudController');
    Route::crud('ot-bonus', 'OtBonusCrudController');
}); // this should be the absolute last line of this file